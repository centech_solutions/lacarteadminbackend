﻿using ChoETL;
using Dapper;
using DocumentFormat.OpenXml.VariantTypes;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WebApplication1.Models;
using static WebApplication1.Models.Admin;
using static WebApplication1.Models.CalculateLocation;

namespace WebApplication1.Repository
{
    public interface IAdminData
    {
        Task<AdminInfo> GetLoginAdmin(LoginAdmin data);
        Task<bool> PostLogAdminLogin(AdminInfo data, string guid);
        Task<bool> PostRegister(SignUpAdmin data);
        Task<List<RestaurantAllData>> GetAllRestaurant();
        Task<int> PutRestaurantInfo(ChangeRestaurantInfo data);
        Task<bool> PostMenuHour(MenuHour data);
        Task<bool> PutMenuHour(MenuHour data);
        Task<bool> DeleteMenuHour(int id, int resid);
        Task<bool> PostMenuGroup(MenuGroup data);
        Task<bool> PutMenuGroup(MenuGroup data);
        Task<bool> DeleteMenuGroup(int id);
        Task<List<ViewMenuHour2>> GetAllMenuHour(int resid);
        Task<List<MenuGroup>> GetMenuGroupByHourID(int hourid);
        Task<RestaurantAllData> GetRestaurantByID(int res_id);
        Task<List<MenuGroup>> GetMenuCategoryByResID(int res_id);
        Task<bool> PutDumpMenuItem(DumpMenuItem data);
        Task<List<ExcelMenuItem>> GetDumpMenuItemByResID(int res_id);
        Task<bool> PutConfirmDumpMenuItem(ConfirmDumpExcel data);
        Task<bool> PutDumpAddon(DumpAddon data);
        Task<List<ViewExcelAddon>> GetDumpAddonByResID(int res_id);
        Task<bool> PutConfirmDumpAddon(ConfirmDumpExcel data);
        Task<bool> PutDumpAddonOption(DumpAddonOption data);
        Task<List<ViewExcelAddonOption>> GetDumpAddonOptionByResID(int res_id);
        Task<bool> PutConfirmDumpAddonOption(ConfirmDumpExcel data);
        Task<bool> PutMenuItem(PutMenuItem data);
        Task<bool> PutAddOn(PutAddOn data);
        Task<bool> PutAddOnOption(PutAddOnOption data);
        Task<List<Order>> GetAllOrder(int? resid, OrderStatus? status);
        Task<OrderDetail> GetOrderByID(int orderid, int resid);
        Task<bool> PutCancelOrder(ChangeStatusOrder data);
        Task<List<Notification>> GetNotiDeviceCustomer(int cusid);
        Task<OrderHistory> GetOrderHistoryType(string dateTime, OrderHistoryType orderHistoryType);
        Task<List<MenuExcel>> GetMenuExcel(int resid);
        Task<List<CustomerDailyReportAppsFlyerIOS>> GetLaCarteCustomerDailyReportAppsFlyerIOS(string results);
        Task<List<RestaurantDailyReportAppsFlyerIOS>> GetLaCarteRestaurantDailyReportAppsFlyerIOS(string results);
    }
    public class AdminRepository : IAdminData
    {
        private readonly IConfiguration configuration;
        public AdminRepository(IConfiguration config)
        {
            this.configuration = config;
        }
        public IDbConnection Connection
        {
            get
            {
                return new SqlConnection(ConfigurationExtensions.GetConnectionString(this.configuration, "DefaultConnection"));
            }
        }

        bool IsVaildEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {

                return false;
            }
        }

        public async Task<AdminInfo> GetLoginAdmin(LoginAdmin data)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    AdminInfo res = new AdminInfo();
                    if ((data.Email == "admin@gmail.com" && data.Application == AppFlag.CMS) || (data.Email == "admin@gmail.com" && data.Application == AppFlag.SalesAdmin))
                    {
                        var _data = "1,2";
                        var query = new StringBuilder();
                        query.AppendLine("SELECT * FROM [dbo].[ADMIN]");
                        query.AppendLine("WHERE Email=@EMAIL AND Password=@PASSWORD COLLATE SQL_Latin1_General_CP1_CS_AS AND IsEnable = 1 AND Application = @APPFLAG");
                        res = await dbConnection.QueryFirstOrDefaultAsync<AdminInfo>(query.ToString(), new { EMAIL = data.Email, PASSWORD = data.Password, APPFLAG = _data });
                    }
                    else
                    {
                        var query = new StringBuilder();
                        query.AppendLine("SELECT * FROM [dbo].[ADMIN]");
                        query.AppendLine("WHERE Email=@EMAIL AND Password=@PASSWORD COLLATE SQL_Latin1_General_CP1_CS_AS AND IsEnable = 1 AND Application = @APPFLAG");
                        res = await dbConnection.QueryFirstOrDefaultAsync<AdminInfo>(query.ToString(), new { EMAIL = data.Email, PASSWORD = data.Password, APPFLAG = data.Application });
                    }                  
                    return res;
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        public async Task<bool> PostLogAdminLogin(AdminInfo data, string guid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("INSERT INTO dbo.Log_ADMIN_Login(Admin_ID,Guid) VALUES");
                    query.AppendLine("(@ADMINID,@GUID)");
                    int status = await dbConnection.ExecuteAsync(query.ToString(), new { ADMINID = data.Admin_ID, GUID = guid });
                    if (status == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> PostRegister(SignUpAdmin data)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    int CheckName = await dbConnection.QueryFirstOrDefaultAsync<int>("SELECT ADMIN_ID FROM [dbo].[ADMIN] WHERE Email= @EMAIL OR PhoneNumber=@PHONENUMBER and Email != '' and PhoneNumber !=''", new { EMAIL = data.Email, PHONENUMBER = data.PhoneNumber });
                    if (CheckName > 0)
                    {
                        return false;
                    }
                    else
                    {
                        int status = await dbConnection.ExecuteAsync("INSERT INTO dbo.ADMIN(Firstname,Lastname,Email,PhoneNumber,Password,Status)VALUES(@Firstname,@Lastname,@Email,@PhoneNumber,@Password,@Status)", new { FIRSTNAME = data.Firstname, LASTNAME = data.Lastname, EMAIL = data.Email, PHONENUMBER = data.PhoneNumber, PASSWORD = data.Password, STATUS = 1 });
                        if (status == 1)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<RestaurantAllData>> GetAllRestaurant()
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT * FROM [dbo].[RESTAURANT] WHERE IsEnable = 1");

                    var results = await dbConnection.QueryAsync<RestaurantAllData>(query.ToString(), new { });
                    var allCuisine = await dbConnection.QueryAsync<Cuisine>("SELECT * FROM [dbo].[Cuisine]", new { });
                    foreach (var item in results)
                    {
                        query = new StringBuilder();
                        query.AppendLine("select (select value from fn_split_string_to_column([Service],',') a where column_id in (1)) as IsPickup,");
                        query.AppendLine("(select value from fn_split_string_to_column([Service],',') a where column_id in (2)) as IsDelivery,");
                        query.AppendLine("(select value from fn_split_string_to_column([Service],',') a where column_id in (3)) as IsDineIn from RESTAURANT where RES_ID = @RES_ID");
                        item.RestaurantService = await dbConnection.QueryFirstOrDefaultAsync<RestaurantService>(query.ToString(), new { RES_ID = item.RES_ID });

                        if (item.Cuisine_ID > 0)
                            item.cuisine = allCuisine.Where(x => x.ID == item.Cuisine_ID).FirstOrDefault();
                    }
                    return results.ToList();
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public async Task<int> PutRestaurantInfo(ChangeRestaurantInfo data)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT * FROM [dbo].[RESTAURANT] WHERE RES_ID=@RES_ID");
                    var _res = await dbConnection.QueryFirstOrDefaultAsync<ChangeRestaurantInfo>(query.ToString(), new { RES_ID = data.RES_ID });
                    data.RestaurantNameJP ??= _res.RestaurantNameJP;
                    data.RestaurantNameEN ??= _res.RestaurantNameEN;
                    data.Address ??= _res.Address;
                    data.City ??= _res.City;
                    data.State ??= _res.State;
                    data.Postcode ??= _res.Postcode;
                    data.Email ??= _res.Email;
                    data.PhoneNumber ??= _res.PhoneNumber;
                    int status = await dbConnection.ExecuteAsync(query.ToString(), new { RES_ID = data.RES_ID });
                    if (status == 0)
                    {
                        return 0;
                    }
                    query = new StringBuilder();
                    query.AppendLine("UPDATE dbo.RESTAURANT SET RestaurantNameJP=@RestaurantNameJP,RestaurantNameEN=@RestaurantNameEN,Address=@Address,City=@City,State=@State,Postcode=@Postcode,Email=@Email,PhoneNumber=@PhoneNumber");
                    query.AppendLine("WHERE RES_ID = @RES_ID");
                    status = await dbConnection.ExecuteAsync(query.ToString(), new
                    {
                        RES_ID = data.RES_ID,
                        RestaurantNameJP = data.RestaurantNameJP,
                        RestaurantNameEN = data.RestaurantNameEN,
                        Address = data.Address,
                        City = data.City,
                        State = data.State,
                        Postcode = data.Postcode,
                        Email = data.Email,
                        PhoneNumber = data.PhoneNumber
                    });
                    if (status == 1)
                    {
                        return 10;
                    }
                    else
                    {
                        return 0;
                    }

                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> PostMenuHour(MenuHour data)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var dayOfweek = (data.IsSun == true ? 1 : 0) + "," + (data.IsMon == true ? 1 : 0) + "," + (data.IsTue == true ? 1 : 0) + "," + (data.IsWed == true ? 1 : 0) + "," + (data.IsThu == true ? 1 : 0) + "," + (data.IsFri == true ? 1 : 0) + "," + (data.IsSat == true ? 1 : 0);
                    var query = new StringBuilder();
                    query.AppendLine("INSERT INTO dbo.MenuHour (Name_EN,Name_JP,Dec_EN,Dec_JP,DayOfWeek,StartTime,EndTime,StartTime2,EndTime2,StartTime3,EndTime3,StartTime4,EndTime4,StartTime5,EndTime5,StartTime6,EndTime6,StartTime7,EndTime7,TaxType,RES_ID)");
                    query.AppendLine("VALUES (@Name_EN,@Name_JP,@Dec_EN,@Dec_JP,@DayOfWeek,convert(time,@StartTime),convert(time,@EndTime),convert(time,@StartTime2),convert(time,@EndTime2),convert(time,@StartTime3),convert(time,@EndTime3),convert(time,@StartTime4),convert(time,@EndTime4),convert(time,@StartTime5),convert(time,@EndTime5),convert(time,@StartTime6),convert(time,@EndTime6),convert(time,@StartTime7),convert(time,@EndTime7),@TaxType,@RES_ID)");
                    int status = await dbConnection.ExecuteAsync(query.ToString(), new
                    {
                        Name_EN = data.Name_EN,
                        Name_JP = data.Name_JP,
                        Dec_EN = data.Desc_EN,
                        Dec_JP = data.Desc_JP,
                        DayOfWeek = dayOfweek,
                        StartTime = data.StartTime,
                        EndTime = data.EndTime,
                        StartTime2 = data.StartTime2,
                        EndTime2 = data.EndTime2,
                        StartTime3 = data.StartTime3,
                        EndTime3 = data.EndTime3,
                        StartTime4 = data.StartTime4,
                        EndTime4 = data.EndTime4,
                        StartTime5 = data.StartTime5,
                        EndTime5 = data.EndTime5,
                        StartTime6 = data.StartTime6,
                        EndTime6 = data.EndTime6,
                        StartTime7 = data.StartTime7,
                        EndTime7 = data.EndTime7,
                        TaxType = data.TaxType,
                        RES_ID = data.RES_ID
                    });
                    if (status == 1)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> PutMenuHour(MenuHour data)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var dayOfweek = (data.IsSun == true ? 1 : 0) + "," + (data.IsMon == true ? 1 : 0) + "," + (data.IsTue == true ? 1 : 0) + "," + (data.IsWed == true ? 1 : 0) + "," + (data.IsThu == true ? 1 : 0) + "," + (data.IsFri == true ? 1 : 0) + "," + (data.IsSat == true ? 1 : 0);
                    var query = new StringBuilder();
                    query.AppendLine("UPDATE dbo.MenuHour SET Name_EN=@Name_EN,Name_JP=@Name_JP,Dec_EN=@Dec_EN,DayOfWeek=@DayOfWeek,");
                    query.AppendLine("StartTime=@StartTime,EndTime=@EndTime,");
                    query.AppendLine("StartTime2=@StartTime2,EndTime2=@EndTime2,");
                    query.AppendLine("StartTime3=@StartTime3,EndTime3=@EndTime3,");
                    query.AppendLine("StartTime4=@StartTime4,EndTime4=@EndTime4,");
                    query.AppendLine("StartTime5=@StartTime5,EndTime5=@EndTime5,");
                    query.AppendLine("StartTime6=@StartTime6,EndTime6=@EndTime6,");
                    query.AppendLine("StartTime7=@StartTime7,EndTime7=@EndTime7,TaxType=@TaxType WHERE ID=@ID");

                    int status = await dbConnection.ExecuteAsync(query.ToString(),
                     new
                     {
                         ID = data.ID,
                         Name_EN = data.Name_EN,
                         Dec_EN = data.Desc_EN,
                         Name_JP = data.Name_JP,
                         Dec_JP = data.Desc_JP,
                         DayOfWeek = dayOfweek,
                         StartTime = data.StartTime,
                         EndTime = data.EndTime,
                         StartTime2 = data.StartTime2,
                         EndTime2 = data.EndTime2,
                         StartTime3 = data.StartTime3,
                         EndTime3 = data.EndTime3,
                         StartTime4 = data.StartTime4,
                         EndTime4 = data.EndTime4,
                         StartTime5 = data.StartTime5,
                         EndTime5 = data.EndTime5,
                         StartTime6 = data.StartTime6,
                         EndTime6 = data.EndTime6,
                         StartTime7 = data.StartTime7,
                         EndTime7 = data.EndTime7,
                         TaxType = data.TaxType
                     });
                    if (status == 1)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> DeleteMenuHour(int id, int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    int status = await dbConnection.ExecuteAsync("UPDATE dbo.MenuHour SET IsEnable=0 WHERE ID=@ID AND RES_ID=@RES_ID",
                                                                new { ID = id, RES_ID = resid });
                    if (status == 1)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<ViewMenuHour2>> GetAllMenuHour(int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var sb = new StringBuilder();
                    sb.AppendLine("select [ID],[Sequence],[Name_EN],[Name_JP],[Dec_EN] as Desc_EN,[Dec_JP] as Desc_JP,");
                    sb.AppendLine("(select value from fn_split_string_to_column([DayOfWeek],',') a where column_id in (1)) as IsSun,");
                    sb.AppendLine("(select value from fn_split_string_to_column([DayOfWeek],',') a where column_id in (2)) as IsMon,");
                    sb.AppendLine("(select value from fn_split_string_to_column([DayOfWeek],',') a where column_id in (3)) as IsTue,");
                    sb.AppendLine("(select value from fn_split_string_to_column([DayOfWeek],',') a where column_id in (4)) as IsWed,");
                    sb.AppendLine("(select value from fn_split_string_to_column([DayOfWeek],',') a where column_id in (5)) as IsThu,");
                    sb.AppendLine("(select value from fn_split_string_to_column([DayOfWeek],',') a where column_id in (6)) as IsFri,");
                    sb.AppendLine("(select value from fn_split_string_to_column([DayOfWeek],',') a where column_id in (7)) as IsSat,");
                    sb.AppendLine("convert(varchar,[StartTime],8) as StartTime,convert(varchar,[EndTime],8) as EndTime,");
                    sb.AppendLine("convert(varchar,[StartTime2],8) as StartTime2,convert(varchar,[EndTime2],8) as EndTime2,");
                    sb.AppendLine("convert(varchar,[StartTime3],8) as StartTime3,convert(varchar,[EndTime3],8) as EndTime3,");
                    sb.AppendLine("convert(varchar,[StartTime4],8) as StartTime4,convert(varchar,[EndTime4],8) as EndTime4,");
                    sb.AppendLine("convert(varchar,[StartTime5],8) as StartTime5,convert(varchar,[EndTime5],8) as EndTime5,");
                    sb.AppendLine("convert(varchar,[StartTime6],8) as StartTime6,convert(varchar,[EndTime6],8) as EndTime6,");
                    sb.AppendLine("convert(varchar,[StartTime7],8) as StartTime7,convert(varchar,[EndTime7],8) as EndTime7,");
                    sb.AppendLine("[CreatedDate] from MenuHour");
                    sb.AppendLine("where RES_ID = @RES_ID and IsEnable = 1 order by [Sequence]");
                    var results = await dbConnection.QueryAsync<ViewMenuHour2>(sb.ToString(), new { RES_ID = resid });
                    var _menuGroup = await dbConnection.QueryAsync<MenuGroup>("SELECT MG.[ID],MG.[Sequence],MG.[Name],MG.[MenuHour_ID],MG.[CreatedDate] FROM [dbo].[MenuGroup] MG inner join MenuHour MH on MG.MenuHour_ID = MH.ID where MH.RES_ID = @RES_ID and MG.IsEnable = 1", new { RES_ID = resid });
                    if (results != null)
                        foreach (var _menuhour in results)
                        {
                            _menuhour.MenuGroups = _menuGroup.Where(x => x.MenuHour_ID == _menuhour.ID).OrderBy(x => x.Sequence).ToList();
                        }
                    return results.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> PostMenuGroup(MenuGroup data)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    int status = await dbConnection.ExecuteAsync("INSERT INTO dbo.MenuGroup (Name,MenuHour_ID) VALUES (@Name,@MenuHour_ID)",
                                                                new { Name = data.Name, MenuHour_ID = data.MenuHour_ID });
                    if (status == 1)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> PutMenuGroup(MenuGroup data)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    int status = await dbConnection.ExecuteAsync("UPDATE dbo.MenuGroup SET Name=@Name,MenuHour_ID=@MenuHour_ID WHERE ID=@ID",
                                                                new { ID = data.ID, Name = data.Name, MenuHour_ID = data.MenuHour_ID });
                    if (status == 1)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> DeleteMenuGroup(int id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    int status = await dbConnection.ExecuteAsync("UPDATE dbo.MenuGroup SET IsEnable=0 WHERE ID=@ID",
                                                                new { ID = id });
                    if (status == 1)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<MenuGroup>> GetMenuGroupByHourID(int hourid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var results = await dbConnection.QueryAsync<MenuGroup>("SELECT * FROM [dbo].[MenuGroup]  where MenuHour_ID = @MenuHour_ID and IsEnable = 1 order by Sequence", new { MenuHour_ID = hourid });

                    return results.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<RestaurantAllData> GetRestaurantByID(int res_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var results = await dbConnection.QueryFirstOrDefaultAsync<RestaurantAllData>("SELECT * FROM [dbo].[RESTAURANT] WHERE RES_ID=@resid", new { resid = res_id });
                    if (results.Cuisine_ID > 0)
                        results.cuisine = await dbConnection.QueryFirstOrDefaultAsync<Cuisine>("SELECT * FROM [dbo].[Cuisine] WHERE ID=@id", new { id = results.Cuisine_ID });
                    results.DeliveryFee = await dbConnection.QueryFirstOrDefaultAsync<DeliveryFee>("SELECT * FROM [dbo].[Cuisine] WHERE ID=@resid", new { resid = res_id });
                    var query = new StringBuilder();
                    query.AppendLine("select (select value from fn_split_string_to_column([Service],',') a where column_id in (1)) as IsPickup,");
                    query.AppendLine("(select value from fn_split_string_to_column([Service],',') a where column_id in (2)) as IsDelivery,");
                    query.AppendLine("(select value from fn_split_string_to_column([Service],',') a where column_id in (3)) as IsDineIn from RESTAURANT where RES_ID = @RES_ID");
                    results.RestaurantService = await dbConnection.QueryFirstOrDefaultAsync<RestaurantService>(query.ToString(), new { RES_ID = res_id });
                    return results;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<MenuGroup>> GetMenuCategoryByResID(int res_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    int _dayOfWeek = (int)DateTime.UtcNow.AddHours(9).DayOfWeek;
                    var query = new StringBuilder();
                    query.AppendLine("select MG.ID,MG.Name from MenuHour MH");
                    query.AppendLine("inner join MenuGroup MG on MG.MenuHour_ID = MH.ID");
                    query.AppendLine("where RES_ID = @resid and MH.IsEnable = 1 and MG.IsEnable = 1");
                    query.AppendLine("and (select value from fn_split_string_to_column([DayOfWeek],',') a where column_id in (DATEPART(dw,GETDATE()))) = 1");
                    switch (_dayOfWeek)
                    {
                        case 0:
                            query.AppendLine("and CONVERT(TIME,GETDATE()) between [StartTime]  AND [EndTime]");
                            break;
                        case 1:
                            query.AppendLine("and CONVERT(TIME,GETDATE()) between [StartTime2]  AND [EndTime2]");
                            break;
                        case 2:
                            query.AppendLine("and CONVERT(TIME,GETDATE()) between [StartTime3]  AND [EndTime3]");
                            break;
                        case 3:
                            query.AppendLine("and CONVERT(TIME,GETDATE()) between [StartTime4]  AND [EndTime4]");
                            break;
                        case 4:
                            query.AppendLine("and CONVERT(TIME,GETDATE()) between [StartTime5]  AND [EndTime5]");
                            break;
                        case 5:
                            query.AppendLine("and CONVERT(TIME,GETDATE()) between [StartTime6]  AND [EndTime6]");
                            break;
                        case 6:
                            query.AppendLine("and CONVERT(TIME,GETDATE()) between [StartTime7]  AND [EndTime7]");
                            break;
                        default:
                            query.AppendLine("and CONVERT(TIME,GETDATE()) between [StartTime]  AND [EndTime]");
                            break;
                    }
                    query.AppendLine("Order by MH.Sequence,MG.Sequence");

                    var results = await dbConnection.QueryAsync<MenuGroup>(query.ToString(), new { resid = res_id });
                    return results.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> PutDumpMenuItem(DumpMenuItem data)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("DELETE [DumpExcel_MenuItem]");
                    query.AppendLine("WHERE RES_ID = @RES_ID");
                    int status = await dbConnection.ExecuteAsync(query.ToString(), new
                    {
                        RES_ID = data.RES_ID
                    });
                    foreach (var item in data.Items)
                    {
                        query = new StringBuilder();
                        query.AppendLine("INSERT INTO [DumpExcel_MenuItem] ([No],[Name_JP],[Description_JP],[Name_EN],[Description_EN],[Price],[Delivery],[Pickup],[DineIn],[CookingTime],[RES_ID],[MenuGroup_ID])");
                        query.AppendLine("VALUES (@NO,@NAMEJP,@DESCJP,@NAMEEN,@DESCEN,@PRICE,@DELIVERY,@PICKUP,@DINEIN,@COOKING,@RESID,@MENUGROUP)");
                        status = await dbConnection.ExecuteAsync(query.ToString(), new
                        {
                            RESID = data.RES_ID,
                            MENUGROUP = data.Category_ID,
                            NO = item.No,
                            NAMEEN = item.Name_EN,
                            DESCEN = item.Description_EN,
                            NAMEJP = item.Name_JP,
                            DESCJP = item.Description_JP,
                            PRICE = item.Price,
                            DELIVERY = item.Delivery,
                            PICKUP = item.Pickup,
                            DINEIN = item.DineIn,
                            COOKING = item.CookingTime
                        });
                        if (status != 1)
                            return false;
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<ExcelMenuItem>> GetDumpMenuItemByResID(int res_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT * FROM [DumpExcel_MenuItem]");
                    query.AppendLine("WHERE RES_ID = @RESID");
                    var results = await dbConnection.QueryAsync<ExcelMenuItem>(query.ToString(), new { RESID = res_id });
                    return results.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> PutConfirmDumpMenuItem(ConfirmDumpExcel data)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    if (data.Status == true)
                    {
                        query = new StringBuilder();
                        query.AppendLine("insert into [MenuItem]");
                        query.AppendLine("SELECT [Name_EN],[Name_JP],Description_EN,Description_JP,0,[Price]");
                        query.AppendLine(",[Delivery],[DineIn],NULL,NULL,[CookingTime],0,[MenuGroup_ID],1,GETDATE(),[Pickup],0");
                        query.AppendLine("FROM [DumpExcel_MenuItem] WHERE RES_ID = @RESID");
                        int status = await dbConnection.ExecuteAsync(query.ToString(), new
                        {
                            RESID = data.RES_ID
                        });
                        if (status != 1)
                            return false;
                        query = new StringBuilder();
                        query.AppendLine("update MenuItem");
                        query.AppendLine("set Sequence = a.RowNo");
                        query.AppendLine("from");
                        query.AppendLine("(SELECT ROW_NUMBER() OVER(PARTITION BY MG.ID ORDER BY MI.ID) AS RowNo ,MI.*");
                        query.AppendLine("from MenuHour MH");
                        query.AppendLine("inner join MenuGroup MG on MG.MenuHour_ID = MH.ID");
                        query.AppendLine("inner join MenuItem MI on MI.MenuGroup_ID = MG.ID");
                        query.AppendLine("where MH.RES_ID = @RESID and MI.Status = 1) a");
                        query.AppendLine("where MenuItem.ID = a.ID");
                        status = await dbConnection.ExecuteAsync(query.ToString(), new
                        {
                            RESID = data.RES_ID
                        });
                        if (status >= 1)
                            return true;
                        else
                            return false;
                    }
                    else
                    {
                        query = new StringBuilder();
                        query.AppendLine("DELETE [DumpExcel_MenuItem]");
                        query.AppendLine("WHERE RES_ID = @RESID");
                        int status = await dbConnection.ExecuteAsync(query.ToString(), new
                        {
                            RESID = data.RES_ID
                        });
                        if (status >= 1)
                            return true;
                        else
                            return false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> PutDumpAddon(DumpAddon data)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("DELETE [DumpExcel_AddOn]");
                    query.AppendLine("WHERE RES_ID = @RESID");
                    int status = await dbConnection.ExecuteAsync(query.ToString(), new
                    {
                        RESID = data.RES_ID
                    });
                    foreach (var item in data.Addons)
                    {
                        query = new StringBuilder();
                        query.AppendLine("INSERT INTO [DumpExcel_AddOn] ([No],[MenuItemNo],[MenuItem_ID],[AddOnName_JP],[AddOnName_EN],[OptionsChoose],[Require],[RES_ID])");
                        query.AppendLine("VALUES (@NO,@MENUITEM,NULL,@NAMEJP,@NAMEEN,@OPTION,@REQUIRE,@RESID)");
                        status = await dbConnection.ExecuteAsync(query.ToString(), new
                        {
                            RESID = data.RES_ID,
                            NO = item.No,
                            MENUITEM = item.MenuItemNo,
                            NAMEEN = item.AddOnName_EN,
                            NAMEJP = item.AddOnName_JP,
                            OPTION = item.OptionsChoose,
                            REQUIRE = item.Require
                        });
                        if (status != 1)
                            return false;
                    }
                    query = new StringBuilder();
                    query.AppendLine("update [DumpExcel_AddOn]");
                    query.AppendLine("set [MenuItem_ID] = a.ID from MenuItem a");
                    query.AppendLine("inner join DumpExcel_MenuItem b on a.Name_JP = b.Name_JP and a.MenuGroup_ID = b.MenuGroup_ID");
                    query.AppendLine("where [DumpExcel_AddOn].RES_ID = @RESID");
                    query.AppendLine("AND DumpExcel_AddOn.RES_ID = b.RES_ID");
                    query.AppendLine("AND DumpExcel_AddOn.MenuItemNo = b.[No]");
                    query.AppendLine("AND a.Status = 1");
                    status = await dbConnection.ExecuteAsync(query.ToString(), new
                    {
                        RESID = data.RES_ID
                    });
                    if (status >= 1)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<ViewExcelAddon>> GetDumpAddonByResID(int res_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT a.*,b.[Name_EN] as MenuItemName_JP FROM [DumpExcel_AddOn] a");
                    query.AppendLine("LEFT JOIN MenuItem b ON a.[MenuItem_ID] = b.[ID]");
                    query.AppendLine("WHERE a.RES_ID = @RESID");
                    var results = await dbConnection.QueryAsync<ViewExcelAddon>(query.ToString(), new { RESID = res_id });
                    return results.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> PutConfirmDumpAddon(ConfirmDumpExcel data)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    if (data.Status == true)
                    {
                        query = new StringBuilder();
                        query.AppendLine("insert into AddOn");
                        query.AppendLine("SELECT [RES_ID],[AddOnName_EN],[AddOnName_JP],[OptionsChoose]");
                        query.AppendLine(",GETDATE(),1,[Require],[MenuItem_ID],1");
                        query.AppendLine("FROM [DumpExcel_AddOn] WHERE RES_ID = @RESID");
                        int status = await dbConnection.ExecuteAsync(query.ToString(), new
                        {
                            RESID = data.RES_ID
                        });
                        if (status >= 1)
                            return true;
                        else
                            return false;
                    }
                    else
                    {
                        query = new StringBuilder();
                        query.AppendLine("DELETE [DumpExcel_AddOn]");
                        query.AppendLine("WHERE RES_ID = @RESID");
                        int status = await dbConnection.ExecuteAsync(query.ToString(), new
                        {
                            RESID = data.RES_ID
                        });
                        if (status >= 1)
                            return true;
                        else
                            return false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> PutDumpAddonOption(DumpAddonOption data)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("DELETE [DumpExcel_AddOnOption]");
                    query.AppendLine("WHERE RES_ID = @RESID");
                    int status = await dbConnection.ExecuteAsync(query.ToString(), new
                    {
                        RESID = data.RES_ID
                    });
                    foreach (var item in data.AddonOptions)
                    {
                        query = new StringBuilder();
                        query.AppendLine("INSERT INTO [DumpExcel_AddOnOption] ([AddOnNo],[AddOn_ID],[Name_JP],[Name_EN],[Price],[RES_ID])");
                        query.AppendLine("VALUES (@ADDON,NULL,@NAMEJP,@NAMEEN,@PRICE,@RESID)");
                        status = await dbConnection.ExecuteAsync(query.ToString(), new
                        {
                            RESID = data.RES_ID,
                            ADDON = item.AddonNo,
                            NAMEEN = item.Name_EN,
                            NAMEJP = item.Name_JP,
                            PRICE = item.Price
                        });
                        if (status != 1)
                            return false;
                    }
                    query = new StringBuilder();
                    query.AppendLine("update [DumpExcel_AddOnOption]");
                    query.AppendLine("set [AddOn_ID] = AddOn.ID from AddOn ");
                    query.AppendLine("inner join DumpExcel_AddOn  on AddOn.Name_JP = DumpExcel_AddOn.[AddOnName_JP] and AddOn.MenuItem_ID = DumpExcel_AddOn.MenuItem_ID");
                    query.AppendLine("where DumpExcel_AddOn.NO = [DumpExcel_AddOnOption].AddOnNo");
                    query.AppendLine("AND DumpExcel_AddOnOption.RES_ID = DumpExcel_AddOn.RES_ID");
                    query.AppendLine("AND [DumpExcel_AddOnOption].RES_ID = @RESID");
                    query.AppendLine("AND AddOn.Status = 1");
                    status = await dbConnection.ExecuteAsync(query.ToString(), new
                    {
                        RESID = data.RES_ID
                    });
                    if (status >= 1)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<ViewExcelAddonOption>> GetDumpAddonOptionByResID(int res_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT a.*,b.[Name_EN] as AddonName_JP FROM [DumpExcel_AddOnOption] a");
                    query.AppendLine("LEFT JOIN [AddOn] b ON a.[AddOn_ID] = b.[ID]");
                    query.AppendLine("WHERE a.RES_ID = @RESID");
                    var results = await dbConnection.QueryAsync<ViewExcelAddonOption>(query.ToString(), new { RESID = res_id });
                    return results.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> PutConfirmDumpAddonOption(ConfirmDumpExcel data)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    if (data.Status == true)
                    {
                        query = new StringBuilder();
                        query.AppendLine("insert into [AddOnOption]");
                        query.AppendLine("SELECT [AddOn_ID],[Name_EN],[Name_JP],[Price],GETDATE(),1,1");
                        query.AppendLine("FROM [DumpExcel_AddOnOption] WHERE RES_ID = @RESID");
                        int status = await dbConnection.ExecuteAsync(query.ToString(), new
                        {
                            RESID = data.RES_ID
                        });
                        if (status >= 1)
                            return true;
                        else
                            return false;
                    }
                    else
                    {
                        query = new StringBuilder();
                        query.AppendLine("DELETE [DumpExcel_AddOnOption]");
                        query.AppendLine("WHERE RES_ID = @RESID OR RES_ID IS NULL");
                        int status = await dbConnection.ExecuteAsync(query.ToString(), new
                        {
                            RESID = data.RES_ID
                        });
                        if (status >= 1)
                            return true;
                        else
                            return false;
                    }
                }
            }

            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> PutMenuItem(PutMenuItem data)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT MI.*,MH.RES_ID FROM MenuHour as MH");
                    query.AppendLine("INNER JOIN MenuGroup as MG ON(MG.MenuHour_ID = MH.ID)");
                    query.AppendLine("INNER JOIN MenuItem as MI ON (MI.MenuGroup_ID = MG.ID)");
                    query.AppendLine("WHERE RES_ID = @RES_ID and MI.[ID] = @MenuItemID");
                    var MenuItem = await dbConnection.QueryFirstOrDefaultAsync<PutMenuItem>(query.ToString(), new { RES_ID = data.RES_ID, MenuItemID = data.ID });
                    data.Name_EN ??= MenuItem.Name_EN;
                    data.Desc_EN ??= MenuItem.Desc_EN;
                    data.Name_JP ??= MenuItem.Name_JP;
                    data.Desc_JP ??= MenuItem.Desc_JP;
                    data.PercentTax ??= MenuItem.PercentTax;
                    data.Price ??= MenuItem.Price;
                    data.Pickup ??= MenuItem.Pickup;
                    data.Delivery ??= MenuItem.Delivery;
                    data.DineIn ??= MenuItem.DineIn;
                    data.ImageM ??= MenuItem.ImageM;
                    data.ImageL ??= MenuItem.ImageL;
                    data.CookingTime ??= MenuItem.CookingTime;
                    data.Sequence ??= MenuItem.Sequence;
                    query = new StringBuilder();
                    query.AppendLine("UPDATE [MenuItem] SET Name_EN = @NAMEEN, DESC_EN = @DESC_EN, Name_JP = @NAMEJP,Desc_JP = @DESC_JP,PercentTax = @PERCENTTAX , Price = @PRICE ,Pickup = @PICKUP, Delivery = @DELIVERY, DineIn = @DINEIN,");
                    query.AppendLine("ImageM = @IMAGEM, ImageL = @IMAGEL, CookingTime = @COOKINGTIME,Sequence = @SEQUENCE");
                    query.AppendLine("WHERE ID = @ID");
                    int status = await dbConnection.ExecuteAsync(query.ToString(), new { ID = data.ID, NAMEEN = data.Name_EN, DESC_EN = data.Desc_EN, NAMEJP = data.Name_JP, DESC_JP = data.Desc_JP, PERCENTTAX = data.PercentTax, PRICE = data.Price, PICKUP = data.Pickup, DELIVERY = data.Delivery, DINEIN = data.DineIn, IMAGEM = data.ImageM, IMAGEL = data.ImageL, COOKINGTIME = data.CookingTime, SEQUENCE = data.Sequence });
                    if (status == 0)
                    {
                        return false;
                    }
                    foreach (var item in data.Addons)
                    {
                        query = new StringBuilder();
                        query.AppendLine("SELECT * FROM [AddOn] WHERE ID = @ADDONID");
                        var addOn = await dbConnection.QueryFirstOrDefaultAsync<PutAddOn>(query.ToString(), new { ADDONID = item.ID });
                        item.Name_EN ??= addOn.Name_EN;
                        item.Name_JP ??= addOn.Name_JP;
                        item.Require ??= addOn.Require;
                        item.OptionCount ??= addOn.OptionCount;
                        item.IsEnable ??= addOn.IsEnable;
                        query = new StringBuilder();
                        query.AppendLine("UPDATE [AddOn] SET Name_EN = @NAMEEN, Name_JP = @NAMEJP,Require = @REQUIRE,OptionCount = @OPTIONCOUNT, IsEnable = @ISENABLE");
                        query.AppendLine("WHERE ID = @ADDONID");
                        status = await dbConnection.ExecuteAsync(query.ToString(), new { ADDONID = item.ID, NAMEEN = item.Name_EN, NAMEJP = item.Name_JP, REQUIRE = item.Require, OPTIONCOUNT = item.OptionCount, ISENABLE = item.IsEnable });
                        if (status == 0)
                        {
                            return false;
                        }
                        foreach (var Item in item.Options)
                        {
                            query = new StringBuilder();
                            query.AppendLine("SELECT * FROM [AddOnOption] WHERE ID = @ADDONOPTIONID");
                            var option = await dbConnection.QueryFirstOrDefaultAsync<PutAddOnOption>(query.ToString(), new { ADDONOPTIONID = Item.ID });
                            Item.Name_EN ??= option.Name_EN;
                            Item.Name_JP ??= option.Name_JP;
                            Item.Price ??= option.Price;
                            Item.IsEnable ??= option.IsEnable;
                            query = new StringBuilder();
                            query.AppendLine("UPDATE [AddOnOption] SET Name_EN = @NAMEEN, Name_JP = @NAMEJP,Price = @PRICE, IsEnable = @ISENABLE");
                            query.AppendLine("WHERE ID = @ADDONOPTIONID");
                            status = await dbConnection.ExecuteAsync(query.ToString(), new { ADDONOPTIONID = Item.ID, NAMEEN = Item.Name_EN, NAMEJP = Item.Name_JP, PRICE = Item.Price, ISENABLE = Item.IsEnable });
                            if (status == 0)
                            {
                                return false;
                            }
                        }
                    }

                    return true;
                }
            }

            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> PutAddOn(PutAddOn data)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT * FROM [AddOn] WHERE ID = @ADDONID and RES_ID = @RES_ID");
                    var AddOn = await dbConnection.QueryFirstOrDefaultAsync<PutAddOn>(query.ToString(), new { ADDONID = data.ID, RES_ID = data.RES_ID });
                    data.Name_EN ??= AddOn.Name_EN;
                    data.Name_JP ??= AddOn.Name_JP;
                    data.Require ??= AddOn.Require;
                    data.OptionCount ??= AddOn.OptionCount;
                    data.IsEnable ??= AddOn.IsEnable;
                    query = new StringBuilder();
                    query.AppendLine("UPDATE [AddOn] SET Name_EN = @NAMEEN, Name_JP = @NAMEJP,Require = @REQUIRE,OptionCount = @OPTIONCOUNT, IsEnable = @ISENABLE");
                    query.AppendLine("WHERE ID = @ADDONID and RES_ID = @RES_ID");
                    int status = await dbConnection.ExecuteAsync(query.ToString(), new { ADDONID = data.ID, RES_ID = data.RES_ID, NAMEEN = data.Name_EN, NAMEJP = data.Name_JP, REQUIRE = data.Require, OPTIONCOUNT = data.OptionCount, ISENABLE = data.IsEnable });
                    if (status == 0)
                    {
                        return false;
                    }
                    foreach (var item in data.Options)
                    {
                        query = new StringBuilder();
                        query.AppendLine("SELECT * FROM AddOnOption WHERE AddOn_ID =  @ADDONID");
                        var option = await dbConnection.QueryFirstOrDefaultAsync<PutAddOnOption>(query.ToString(), new { ADDONID = item.AddOn_ID });
                        item.Name_EN ??= option.Name_EN;
                        item.Name_JP ??= option.Name_JP;
                        item.Price ??= option.Price;
                        item.IsEnable ??= option.IsEnable;
                        query = new StringBuilder();
                        query.AppendLine("UPDATE [AddOnOption] SET Name_EN = @NAMEEN, Name_JP = @NAMEJP,Price = @PRICE, IsEnable = @ISENABLE");
                        query.AppendLine("WHERE  ID = @ADDONOPTIONID and AddOn_ID = @ADDONID");
                        status = await dbConnection.ExecuteAsync(query.ToString(), new { ADDONOPTIONID = item.ID, ADDONID = item.AddOn_ID, NAMEEN = item.Name_EN, NAMEJP = item.Name_JP, PRICE = item.Price, ISENABLE = item.IsEnable });
                        if (status == 0)
                        {
                            return false;
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        public async Task<bool> PutAddOnOption(PutAddOnOption data)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT * FROM AddOnOption WHERE  ID = @ADDONOPTIONID and AddOn_ID = @ADDONID");
                    var AddOnOption = await dbConnection.QueryFirstOrDefaultAsync<PutAddOnOption>(query.ToString(), new { ADDONOPTIONID = data.ID, ADDONID = data.AddOn_ID });
                    data.Name_EN ??= AddOnOption.Name_EN;
                    data.Name_JP ??= AddOnOption.Name_JP;
                    data.Price ??= AddOnOption.Price;
                    data.IsEnable ??= AddOnOption.IsEnable;
                    query = new StringBuilder();
                    query.AppendLine("UPDATE [AddOnOption] SET Name_EN = @NAMEEN, Name_JP = @NAMEJP,Price = @PRICE, IsEnable = @ISENABLE");
                    query.AppendLine("WHERE ID = @ADDONOPTIONID and AddOn_ID = @ADDONID");
                    var status = await dbConnection.ExecuteAsync(query.ToString(), new { ADDONOPTIONID = data.ID, ADDONID = data.AddOn_ID, NAMEEN = data.Name_EN, NAMEJP = data.Name_JP, PRICE = data.Price, ISENABLE = data.IsEnable });
                    if (status == 0)
                    {
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public async Task<List<Order>> GetAllOrder(int? resid, OrderStatus? status)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT a.*,B.Location as RestaurantLocation, C.Location as CustomerLocation FROM [Order] A");
                    query.AppendLine("INNER JOIN RESTAURANT B ON A.RES_ID = B.RES_ID");
                    query.AppendLine("INNER JOIN CUSTOMER C ON A.CUS_ID = C.CUS_ID");
                    if (resid != null || status != null)
                    {
                        query.AppendLine("where");
                        if (resid != null && status != null)
                            query.AppendLine("B.RES_ID = " + resid + " AND A.Status = " + (int)status);
                        else if (resid != null && status == null)
                            query.AppendLine("B.RES_ID = " + resid);
                        else
                            query.AppendLine("A.Status = " + (int)status);
                    }
                    else
                        query.AppendLine("where A.Status < " + (int)OrderStatus.Complete + " AND A.STATUS != " + (int)OrderStatus.Paid + "");
                    var results = await dbConnection.QueryAsync<Order>(query.ToString(), new { });
                    if (results != null)
                        foreach (var item in results)
                        {
                            query = new StringBuilder();
                            query.AppendLine("SELECT [RES_ID],[OwnerFirstName],[OwnerLastName],[RestaurantNameJP],[Logo],[Location],[Address],[City]");
                            query.AppendLine(",[State],[Postcode],[ConfirmAddress],[Email],[OfficialPhone],[ConfirmOfficialPhone],[PhoneNumber],[Rate],[PriceRate],[Password],[BankName]");
                            query.AppendLine(",[BankAccountNumber],[BankRoutingNumber],[TaxClassification_ID],[BankAccountHolder],[CompanyName],[CompanyAddress],[CompanyCity]");
                            query.AppendLine(",[CompanyState],[CompanyPostCode],[Service],[DeliveryDistance],[MinOrderSize],[DefaultCookingTime],[DefaultAcceptTime],[TaxNumber]");
                            query.AppendLine(",[Cuisine_ID],[CuisineOtherName],[Status],[CreatedDate],[UpdatedDate],[IsEnable],[OTP_validate]");
                            query.AppendLine(",[RateCount],[AlternativeVerify],[RestaurantNameEN],[DineInPassword],[PlaceId],[NotiToken],[NotiIsEnable]");
                            query.AppendLine(",[AccountID],[AppLanguage],[OrderReminder],[OrderRemindService]");
                            query.AppendLine("FROM [dbo].[RESTAURANT] WHERE RES_ID=@resid");
                            item.Restaurant = await dbConnection.QueryFirstOrDefaultAsync<RestaurantInfo>(query.ToString(), new { resid = item.RES_ID });
                            item.Distance = (item.CustomerLocation == null || item.RestaurantLocation == null ? 0m : (decimal)CalculateDistance(GetLocation(item.CustomerLocation), GetLocation(item.RestaurantLocation)));
                            query = new StringBuilder();
                            query.AppendLine("SELECT a.ID,b.Name_EN,b.Name_JP,b.Desc_EN,b.Desc_JP,a.Amount,a.Price");
                            query.AppendLine(",a.Comment,b.MenuGroup_ID");
                            query.AppendLine("FROM [OrderItem] a");
                            query.AppendLine("inner join [MenuItem] b on a.MenuItem_ID = b.ID");
                            query.AppendLine("WHERE a.Order_ID = @ORDER");
                            var orderItems = await dbConnection.QueryAsync<ViewOrderItem>(query.ToString(), new { ORDER = item.ID });
                            item.Items = orderItems.ToList();
                            if (item.Items.Count > 0)
                            {
                                query = new StringBuilder();
                                query.AppendLine("select b.*,a.ID as OrderItem_ID from OrderItem a");
                                query.AppendLine("inner join AddOn b on a.MenuItem_ID = b.MenuItem_ID");
                                query.AppendLine("where a.Order_ID = @ORDERID");
                                var listAddOn = await dbConnection.QueryAsync<ViewAddOnOrderItem>(query.ToString(), new { ORDERID = item.ID });
                                if (listAddOn.ToList().Count > 0)
                                {
                                    var addonStrings = listAddOn.Select(i => i.ID.ToString(CultureInfo.InvariantCulture)).Aggregate((s1, s2) => s1 + ", " + s2);
                                    query = new StringBuilder();
                                    query.AppendLine("SELECT a.ID as OrderItem_ID,b.[ID],c.AddOn_ID,c.Name_EN,c.Name_JP,b.[Price]");
                                    query.AppendLine("FROM [OrderItem] a");
                                    query.AppendLine("inner join [OrderItemAddOn] b on a.Id = b.OrderItem_ID");
                                    query.AppendLine("inner join AddOnOption c on b.AddOnOption_ID = c.ID");
                                    query.AppendLine("WHERE AddOn_ID IN (" + addonStrings + ") and a.Order_ID = @ORDERID");
                                    var listAddOnOption = await dbConnection.QueryAsync<ViewAddOnOrderOption>(query.ToString(), new { ORDERID = item.ID });
                                    if (listAddOn != null)
                                        foreach (var _item in listAddOn)
                                        {
                                            _item.Options = listAddOnOption.Where(x => x.OrderItem_ID == _item.OrderItem_ID && x.AddOn_ID == item.ID).ToList();
                                        }
                                }
                                if (item.Items != null)
                                    foreach (var _item in item.Items)
                                    {
                                        _item.AddOns = listAddOn.Where(x => x.OrderItem_ID == item.ID).ToList();
                                    }
                            }
                        }
                    return results.OrderByDescending(x => x.CreatedDate).ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<OrderDetail> GetOrderByID(int orderid, int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT a.*,B.Location as RestaurantLocation, C.Location as CustomerLocation FROM [Order] A");
                    query.AppendLine("INNER JOIN RESTAURANT B ON A.RES_ID = B.RES_ID");
                    query.AppendLine("INNER JOIN CUSTOMER C ON A.CUS_ID = C.CUS_ID");
                    query.AppendLine("where A.RES_ID = @RES_ID and A.ID = @ORDER_ID");
                    var result = await dbConnection.QueryFirstOrDefaultAsync<OrderDetail>(query.ToString(), new { RES_ID = resid, ORDER_ID = orderid });
                    result.OrderAddress = await dbConnection.QueryFirstOrDefaultAsync<OrderAddress>("SELECT * FROM OrderAddress where Order_ID = @ORDER_ID", new { ORDER_ID = orderid });
                    result.Distance = (result.OrderAddress == null || result.RestaurantLocation == null ? 0m : (decimal)CalculateDistance(GetLocation(result.OrderAddress.Location), GetLocation(result.RestaurantLocation)));
                    query = new StringBuilder();
                    query.AppendLine("SELECT a.ID as Order_ID,b.Rate as FoodRate,b.Comment as FoodComment,");
                    query.AppendLine("c.Rate as AgentRate,c.Comment as AgentComment FROM [Order] a");
                    query.AppendLine("LEFT JOIN RestaurantRate b ON a.ID = b.Order_ID AND a.RES_ID = b.RES_ID");
                    query.AppendLine("LEFT JOIN AgentRate c ON a.ID = c.Order_ID AND a.Agent_ID = c.Agent_ID");
                    query.AppendLine("WHERE a.ID = @ORDER_ID AND a.RES_ID = @RES_ID");
                    result.OrderRate = await dbConnection.QueryFirstOrDefaultAsync<OrderRate>(query.ToString(), new { RES_ID = resid, ORDER_ID = orderid });
                    query = new StringBuilder();
                    query.AppendLine("SELECT a.ID,b.Name_EN,b.Name_JP,b.Desc_EN,b.Desc_JP,a.Amount,a.Price");
                    query.AppendLine(",a.Comment,b.MenuGroup_ID");
                    query.AppendLine("FROM [OrderItem] a");
                    query.AppendLine("inner join [MenuItem] b on a.MenuItem_ID = b.ID");
                    query.AppendLine("WHERE a.Order_ID = @ORDER");
                    var orderItems = await dbConnection.QueryAsync<ViewOrderItem>(query.ToString(), new { ORDER = orderid });
                    result.Items = orderItems.ToList();
                    if (result.Items.Count > 0)
                    {
                        query = new StringBuilder();
                        query.AppendLine("select b.*,a.ID as OrderItem_ID from OrderItem a");
                        query.AppendLine("inner join AddOn b on a.MenuItem_ID = b.MenuItem_ID");
                        query.AppendLine("where a.Order_ID = @ORDERID");
                        var listAddOn = await dbConnection.QueryAsync<ViewAddOnOrderItem>(query.ToString(), new { ORDERID = orderid });
                        if (listAddOn.ToList().Count > 0)
                        {
                            var addonStrings = listAddOn.Select(i => i.ID.ToString(CultureInfo.InvariantCulture)).Aggregate((s1, s2) => s1 + ", " + s2);
                            query = new StringBuilder();
                            query.AppendLine("SELECT a.ID as OrderItem_ID,b.[ID],c.AddOn_ID,c.Name_EN,c.Name_JP,b.[Price]");
                            query.AppendLine("FROM [OrderItem] a");
                            query.AppendLine("inner join [OrderItemAddOn] b on a.Id = b.OrderItem_ID");
                            query.AppendLine("inner join AddOnOption c on b.AddOnOption_ID = c.ID");
                            query.AppendLine("WHERE AddOn_ID IN (" + addonStrings + ") and a.Order_ID = @ORDERID");
                            var listAddOnOption = await dbConnection.QueryAsync<ViewAddOnOrderOption>(query.ToString(), new { ORDERID = orderid });
                            if (listAddOn != null)
                                foreach (var item in listAddOn)
                                {
                                    item.Options = listAddOnOption.Where(x => x.OrderItem_ID == item.OrderItem_ID && x.AddOn_ID == item.ID).ToList();
                                }
                        }
                        if (result.Items != null)
                            foreach (var item in result.Items)
                            {
                                item.AddOns = listAddOn.Where(x => x.OrderItem_ID == item.ID).ToList();
                            }
                    }
                    if (result.Agent_ID != null && result.Agent_ID > 0)
                    {
                        query = new StringBuilder();
                        query.AppendLine("SELECT A.*,B.Status as RestaurantActive FROM Agent A");
                        query.AppendLine("INNER JOIN RestaurantAgent B on A.Agent_ID = B.Agent_ID");
                        query.AppendLine("WHERE B.RES_ID = @RESID AND A.Agent_ID = @AGENT");
                        result.Agent = await dbConnection.QueryFirstOrDefaultAsync<AgentInfo>(query.ToString(), new { RESID = result.RES_ID, AGENT = result.Agent_ID });
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> PutCancelOrder(ChangeStatusOrder data)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    int statusOrder = (int)OrderStatus.RestaurantCancel;
                    var query = new StringBuilder();
                    query.AppendLine("SELECT a.*,B.Location as RestaurantLocation, C.Location as CustomerLocation FROM [Order] A");
                    query.AppendLine("INNER JOIN RESTAURANT B ON A.RES_ID = B.RES_ID");
                    query.AppendLine("INNER JOIN CUSTOMER C ON A.CUS_ID = C.CUS_ID");
                    query.AppendLine("where A.RES_ID = @RES_ID and A.ID = @ORDER_ID");
                    var order = await dbConnection.QueryFirstOrDefaultAsync<OrderDetail>(query.ToString(), new { RES_ID = data.RES_ID, ORDER_ID = data.Order_ID });
                    if (order == null)
                        return false;
                    query = new StringBuilder();
                    query.AppendLine("UPDATE [Order] SET [Status] = @STATUS,[Complete_Time] = GETDATE()");
                    query.AppendLine("WHERE ID = @ORDERID AND RES_ID = @RESID");
                    int status = await dbConnection.ExecuteAsync(query.ToString(), new { STATUS = statusOrder, ORDERID = data.Order_ID, data.RES_ID });
                    if (status == 1)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<Notification>> GetNotiDeviceCustomer(int cusid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT a.[Device_ID],NotiIsEnable as IsEnable");
                    query.AppendLine("FROM [CustomerNotiDevice] a ");
                    query.AppendLine("inner join CUSTOMER b on a.CUS_ID = b.CUS_ID");
                    query.AppendLine("WHERE a.CUS_ID = @CUSID");
                    query.AppendLine("group by a.[Device_ID],NotiIsEnable");
                    var results = await dbConnection.QueryAsync<Notification>(query.ToString(), new { CUSID = cusid });
                    return results.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public async Task<OrderHistory> GetOrderHistoryType(string dateTime, OrderHistoryType orderHistoryType)
        {
            try
            {

                using (IDbConnection dbConnection = Connection)
                {
                    var results = new OrderHistory();
                    var query = new StringBuilder();
                    switch (orderHistoryType)
                    {
                        case OrderHistoryType.Daily:


                            if (dateTime == null)
                            {
                                query.AppendLine("SELECT FORMAT(CAST(A.CreatedDate AS DATE), 'd MMM yyy') AS Date,a.RES_ID,b.RestaurantNameEN,b.RestaurantNameJP,count(ID) as 'OrderCurrentCount' from[Order] a ");
                                query.AppendLine("INNER JOIN RESTAURANT b ON a.RES_ID = b.RES_ID");
                                query.AppendLine("WHERE a.CreatedDate > convert(date, getdate()) and a.RES_ID not in (250, 83)");
                            }
                            else
                            {
                                query.AppendLine("SELECT FORMAT(CAST(A.CreatedDate AS DATE), 'd MMM yyy') AS Date,a.RES_ID,b.RestaurantNameEN,b.RestaurantNameJP,count(ID) as 'OrderCurrentCount' from[Order] a ");
                                query.AppendLine("INNER JOIN RESTAURANT b ON a.RES_ID = b.RES_ID");
                                query.AppendLine("WHERE a.CreatedDate > convert(date, '" + dateTime + "',103) and a.RES_ID not in (250, 83)");
                            }
                            query.AppendLine("group by a.RES_ID,b.RestaurantNameEN,b.RestaurantNameJP,FORMAT(CAST(a.CreatedDate AS DATE), 'd MMM yyy')");
                            var orderCurrents = await dbConnection.QueryAsync<OrderCurrent>(query.ToString());
                            results.OrderCurrents = orderCurrents.ToList();
                            results.OrderCurrentSum = orderCurrents.Sum(x => x.OrderCurrentCount);
                            query = new StringBuilder();
                            if (dateTime == null)
                            {
                                query.AppendLine("select FORMAT(CAST(GETDATE() AS DATE), 'd MMM yyy') AS Date,a.RES_ID,b.RestaurantNameEN,b.RestaurantNameJP,count(ID) as 'OrderCancelledCount' from[Order] a ");
                                query.AppendLine("inner join RESTAURANT b on a.RES_ID = b.RES_ID");
                                query.AppendLine("where a.CreatedDate > convert(date, getdate()) and a.[Status] in (98,99) and a.RES_ID not in (250,83)");
                            }
                            else
                            {
                                query.AppendLine("select FORMAT(CAST(convert(date, '" + dateTime + "',103) AS DATE), 'd MMM yyy') AS Date,a.RES_ID,b.RestaurantNameEN,b.RestaurantNameJP,count(ID) as 'OrderCancelledCount' from[Order] a ");
                                query.AppendLine("inner join RESTAURANT b on a.RES_ID = b.RES_ID");
                                query.AppendLine("where a.CreatedDate > convert(date, '" + dateTime + "',103) and a.[Status] in (98,99) and a.RES_ID not in (250,83)");
                            }
                            query.AppendLine("group by a.RES_ID,b.RestaurantNameEN,b.RestaurantNameJP,FORMAT(CAST(a.CreatedDate AS DATE), 'd MMM yyy')");
                            var orderCancel = await dbConnection.QueryAsync<OrderCancel>(query.ToString());
                            results.OrderCancels = orderCancel.ToList();
                            results.OrderCancelSum = orderCancel.Sum(x => x.OrderCancelledCount);
                            query = new StringBuilder();
                            if (dateTime == null)
                            {
                                query.AppendLine("select FORMAT(CAST(GETDATE() AS DATE), 'd MMM yyy') AS Date,a.RES_ID,b.RestaurantNameEN,b.RestaurantNameJP,count(ID) as 'OrderCompletedCount' from[Order] a ");
                                query.AppendLine("inner join RESTAURANT b on a.RES_ID = b.RES_ID");
                                query.AppendLine("where a.CreatedDate > convert(date,getdate()) and a.[Status] in (20,9) and a.RES_ID not in (250,83)");
                            }
                            else
                            {
                                query.AppendLine("select FORMAT(CAST(convert(date,'" + dateTime + "',103) AS DATE), 'd MMM yyy') AS Date,a.RES_ID,b.RestaurantNameEN,b.RestaurantNameJP,count(ID) as 'OrderCompletedCount' from[Order] a ");
                                query.AppendLine("inner join RESTAURANT b on a.RES_ID = b.RES_ID");
                                query.AppendLine("where a.CreatedDate > convert(date,'" + dateTime + "',103) and a.[Status] in (20,9) and a.RES_ID not in (250,83)");
                            }
                            query.AppendLine("group by a.RES_ID,b.RestaurantNameEN,b.RestaurantNameJP,FORMAT(CAST(a.CreatedDate AS DATE), 'd MMM yyy') ");
                            var orderCompleteds = await dbConnection.QueryAsync<OrderCompleted>(query.ToString());
                            results.OrderCompletes = orderCompleteds.ToList();
                            results.OrderCompleteSum = orderCompleteds.Sum(x => x.OrderCompletedCount);
                            query = new StringBuilder();

                            if (dateTime == null)
                            {
                                query.AppendLine("select FORMAT(CAST(GETDATE() AS DATE), 'd MMM yyy') AS Date,a.RES_ID,b.RestaurantNameEN,b.RestaurantNameJP,count(ID) as 'OrderPendingCount' from[Order] a ");
                                query.AppendLine("inner join RESTAURANT b on a.RES_ID = b.RES_ID");
                                query.AppendLine("where a.CreatedDate > convert(date, getdate()) and a.[Status] < 20 and a.[Status] != 9  and a.RES_ID not in (250,83)");
                            }
                            else
                            {
                                query.AppendLine("select FORMAT(CAST(convert(date,'" + dateTime + "',103) AS DATE), 'd MMM yyy') AS Date,a.RES_ID,b.RestaurantNameEN,b.RestaurantNameJP,count(ID) as 'OrderPendingCount' from[Order] a ");
                                query.AppendLine("inner join RESTAURANT b on a.RES_ID = b.RES_ID");
                                query.AppendLine("where a.CreatedDate > convert(date,'" + dateTime + "',103) and a.[Status] < 20 and a.[Status] != 9  and a.RES_ID not in (250,83)");
                            }
                            query.AppendLine("group by a.RES_ID,b.RestaurantNameEN,b.RestaurantNameJP,FORMAT(CAST(a.CreatedDate AS DATE), 'd MMM yyy')");
                            var orderPendings = await dbConnection.QueryAsync<OrderPending>(query.ToString());
                            results.OrderPendings = orderPendings.ToList();
                            results.OrderPendingSum = orderPendings.Sum(x => x.OrderPendingCount);
                            break;
                        case OrderHistoryType.Weekly:
                            query = new StringBuilder();
                            query.AppendLine("SELECT a.RES_ID,FORMAT(DATEADD(WEEK, DATEPART(WEEK, a.CreatedDate) - 1, DATEADD(wk, DATEDIFF(wk, -1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0)), 0)), 'dd MMM') + ");
                            query.AppendLine("' - ' + FORMAT(DATEADD(WEEK,DATEPART(WEEK,a.CreatedDate)-1, DATEADD(wk, DATEDIFF(wk,-1,DATEADD(yy, DATEDIFF(yy,0,GETDATE()), 0)), 0)) + 6, 'dd MMM') AS Date,");
                            query.AppendLine("b.RestaurantNameEN,b.RestaurantNameJP,COUNT(ID) AS 'OrderCurrentCount'");
                            query.AppendLine("from [Order] a inner join RESTAURANT b on a.RES_ID = b.RES_ID");
                            if (dateTime == null)
                            {
                                query.AppendLine("WHERE a.CreatedDate > convert(date, getdate()) and a.RES_ID not in (250, 83)");
                            }
                            else
                            {
                                query.AppendLine("WHERE a.CreatedDate > convert(date, '" + dateTime + "',103) and a.RES_ID not in (250, 83)");
                            }
                            query.AppendLine("AND format(CAST(a.CreatedDate AS DATE),'yyy') = format(CAST(GETDATE() AS DATE),'yyy')");
                            query.AppendLine("GROUP BY a.RES_ID,b.RestaurantNameEN,b.RestaurantNameJP,DATEPART(week, a.CreatedDate)");
                            query.AppendLine("ORDER BY DATEPART(week, a.CreatedDate)");
                            var orderCurrentsWeek = await dbConnection.QueryAsync<OrderCurrent>(query.ToString());
                            results.OrderCurrents = orderCurrentsWeek.ToList();
                            results.OrderCurrentSum = orderCurrentsWeek.Sum(x => x.OrderCurrentCount);
                            query = new StringBuilder();
                            query.AppendLine("SELECT a.RES_ID,FORMAT(DATEADD(WEEK, DATEPART(WEEK, a.CreatedDate) - 1, DATEADD(wk, DATEDIFF(wk, -1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0)), 0)), 'dd MMM') + ");
                            query.AppendLine("' - ' + FORMAT(DATEADD(WEEK,DATEPART(WEEK,a.CreatedDate)-1, DATEADD(wk, DATEDIFF(wk,-1,DATEADD(yy, DATEDIFF(yy,0,GETDATE()), 0)), 0)) + 6, 'dd MMM') AS Date,");
                            query.AppendLine("b.RestaurantNameEN,b.RestaurantNameJP,COUNT(ID) AS 'OrderCancelledCount'");
                            query.AppendLine("from [Order] a inner join RESTAURANT b on a.RES_ID = b.RES_ID");
                            if (dateTime == null)
                            {
                                query.AppendLine("where a.CreatedDate > convert(date, getdate()) and a.[Status] in (98,99) and a.RES_ID not in (250,83)");
                            }
                            else
                            {
                                query.AppendLine("where a.CreatedDate > convert(date, '" + dateTime + "',103) and a.[Status] in (98,99) and a.RES_ID not in (250,83)");
                            }
                            query.AppendLine("AND format(CAST(a.CreatedDate AS DATE),'yyy') = format(CAST(GETDATE() AS DATE),'yyy')");
                            query.AppendLine("GROUP BY a.RES_ID,b.RestaurantNameEN,b.RestaurantNameJP,DATEPART(week, a.CreatedDate)");
                            query.AppendLine("ORDER BY DATEPART(week, a.CreatedDate)");
                            var orderCancelsWeek = await dbConnection.QueryAsync<OrderCancel>(query.ToString());
                            results.OrderCancels = orderCancelsWeek.ToList();
                            results.OrderCancelSum = orderCancelsWeek.Sum(x => x.OrderCancelledCount);
                            query = new StringBuilder();
                            query.AppendLine("SELECT a.RES_ID,FORMAT(DATEADD(WEEK, DATEPART(WEEK, a.CreatedDate) - 1, DATEADD(wk, DATEDIFF(wk, -1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0)), 0)), 'dd MMM') + ");
                            query.AppendLine("' - ' + FORMAT(DATEADD(WEEK,DATEPART(WEEK,a.CreatedDate)-1, DATEADD(wk, DATEDIFF(wk,-1,DATEADD(yy, DATEDIFF(yy,0,GETDATE()), 0)), 0)) + 6, 'dd MMM') AS Date,");
                            query.AppendLine("b.RestaurantNameEN,b.RestaurantNameJP,COUNT(ID) AS 'OrderCompletedCount'");
                            query.AppendLine("from [Order] a inner join RESTAURANT b on a.RES_ID = b.RES_ID");
                            if (dateTime == null)
                            {
                                query.AppendLine("where a.CreatedDate > convert(date,getdate()) and a.[Status] in (20,9) and a.RES_ID not in (250,83)");
                            }
                            else
                            {
                                query.AppendLine("where a.CreatedDate > convert(date,'" + dateTime + "',103) and a.[Status] in (20,9) and a.RES_ID not in (250,83)");
                            }
                            query.AppendLine("AND format(CAST(a.CreatedDate AS DATE),'yyy') = format(CAST(GETDATE() AS DATE),'yyy')");
                            query.AppendLine("GROUP BY a.RES_ID,b.RestaurantNameEN,b.RestaurantNameJP,DATEPART(week, a.CreatedDate)");
                            query.AppendLine("ORDER BY DATEPART(week, a.CreatedDate)");
                            var orderCompletedsWeek = await dbConnection.QueryAsync<OrderCompleted>(query.ToString());
                            results.OrderCompletes = orderCompletedsWeek.ToList();
                            results.OrderCompleteSum = orderCompletedsWeek.Sum(x => x.OrderCompletedCount);
                            query = new StringBuilder();
                            query.AppendLine("SELECT a.RES_ID,FORMAT(DATEADD(WEEK, DATEPART(WEEK, a.CreatedDate) - 1, DATEADD(wk, DATEDIFF(wk, -1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0)), 0)), 'dd MMM') + ");
                            query.AppendLine("' - ' + FORMAT(DATEADD(WEEK,DATEPART(WEEK,a.CreatedDate)-1, DATEADD(wk, DATEDIFF(wk,-1,DATEADD(yy, DATEDIFF(yy,0,GETDATE()), 0)), 0)) + 6, 'dd MMM') AS Date,");
                            query.AppendLine("b.RestaurantNameEN,b.RestaurantNameJP,COUNT(ID) AS 'OrderPendingCount'");
                            query.AppendLine("from [Order] a inner join RESTAURANT b on a.RES_ID = b.RES_ID");
                            if (dateTime == null)
                            {
                                query.AppendLine("where a.CreatedDate > convert(date, getdate()) and a.[Status] < 20 and a.[Status] != 9  and a.RES_ID not in (250,83)");
                            }
                            else
                            {
                                query.AppendLine("where a.CreatedDate > convert(date, '" + dateTime + "',103) and a.[Status] < 20 and a.[Status] != 9  and a.RES_ID not in (250,83)");
                            }
                            query.AppendLine("AND format(CAST(a.CreatedDate AS DATE),'yyy') = format(CAST(GETDATE() AS DATE),'yyy')");
                            query.AppendLine("GROUP BY a.RES_ID,b.RestaurantNameEN,b.RestaurantNameJP,DATEPART(week, a.CreatedDate)");
                            query.AppendLine("ORDER BY DATEPART(week, a.CreatedDate)");
                            var orderPendingsWeek = await dbConnection.QueryAsync<OrderPending>(query.ToString());
                            results.OrderPendings = orderPendingsWeek.ToList();
                            results.OrderPendingSum = orderPendingsWeek.Sum(x => x.OrderPendingCount);
                            break;
                        case OrderHistoryType.Monthly:
                            query = new StringBuilder();
                            query.AppendLine("SELECT a.RES_ID,FORMAT(CAST(a.CreatedDate AS DATE), 'MMM') AS Date, COUNT(ID) AS 'OrderCurrentCount',");
                            query.AppendLine("b.RestaurantNameEN,b.RestaurantNameJP");
                            query.AppendLine("from[Order] a inner join RESTAURANT b on a.RES_ID = b.RES_ID");
                            if (dateTime == null)
                            {
                                query.AppendLine("WHERE a.CreatedDate > convert(date, getdate()) and a.RES_ID not in (250, 83)");
                            }
                            else
                            {
                                query.AppendLine("WHERE a.CreatedDate > convert(date, '" + dateTime + "',103) and a.RES_ID not in (250, 83)");
                            }
                            query.AppendLine("AND format(CAST(a.CreatedDate AS DATE),'yyy') = format(CAST(GETDATE() AS DATE), 'yyy')");
                            query.AppendLine("GROUP BY a.RES_ID,format(CAST(a.CreatedDate AS DATE), 'MMM'),b.RestaurantNameEN,b.RestaurantNameJP");
                            query.AppendLine("ORDER BY format(CAST(a.CreatedDate AS DATE), 'MMM') DESC");
                            var orderCurrentsMonth = await dbConnection.QueryAsync<OrderCurrent>(query.ToString());
                            results.OrderCurrents = orderCurrentsMonth.ToList();
                            results.OrderCurrentSum = orderCurrentsMonth.Sum(x => x.OrderCurrentCount);
                            query = new StringBuilder();
                            query.AppendLine("SELECT a.RES_ID,FORMAT(CAST(a.CreatedDate AS DATE), 'MMM') AS Date, COUNT(ID) AS 'OrderCancelledCount',");
                            query.AppendLine("b.RestaurantNameEN,b.RestaurantNameJP");
                            query.AppendLine("from[Order] a inner join RESTAURANT b on a.RES_ID = b.RES_ID");
                            if (dateTime == null)
                            {
                                query.AppendLine("where a.CreatedDate > convert(date, getdate()) and a.[Status] in (98,99) and a.RES_ID not in (250,83)");
                            }
                            else
                            {
                                query.AppendLine("where a.CreatedDate > convert(date, '" + dateTime + "',103) and a.[Status] in (98,99) and a.RES_ID not in (250,83)");
                            }
                            query.AppendLine("AND format(CAST(a.CreatedDate AS DATE),'yyy') = format(CAST(GETDATE() AS DATE), 'yyy')");
                            query.AppendLine("GROUP BY a.RES_ID,format(CAST(a.CreatedDate AS DATE), 'MMM'),b.RestaurantNameEN,b.RestaurantNameJP");
                            query.AppendLine("ORDER BY format(CAST(a.CreatedDate AS DATE), 'MMM') DESC");
                            var orderCancelsMonth = await dbConnection.QueryAsync<OrderCancel>(query.ToString());
                            results.OrderCancels = orderCancelsMonth.ToList();
                            results.OrderCancelSum = orderCancelsMonth.Sum(x => x.OrderCancelledCount);
                            query = new StringBuilder();
                            query.AppendLine("SELECT a.RES_ID,FORMAT(CAST(a.CreatedDate AS DATE), 'MMM') AS Date, COUNT(ID) AS 'OrderCompletes',");
                            query.AppendLine("b.RestaurantNameEN,b.RestaurantNameJP");
                            query.AppendLine("from[Order] a inner join RESTAURANT b on a.RES_ID = b.RES_ID");
                            if (dateTime == null)
                            {
                                query.AppendLine("where a.CreatedDate > convert(date,getdate()) and a.[Status] in (20,9) and a.RES_ID not in (250,83)");
                            }
                            else
                            {
                                query.AppendLine("where a.CreatedDate > convert(date,'" + dateTime + "',103) and a.[Status] in (20,9) and a.RES_ID not in (250,83)");
                            }
                            query.AppendLine("AND format(CAST(a.CreatedDate AS DATE),'yyy') = format(CAST(GETDATE() AS DATE), 'yyy')");
                            query.AppendLine("GROUP BY a.RES_ID,format(CAST(a.CreatedDate AS DATE), 'MMM'),b.RestaurantNameEN,b.RestaurantNameJP");
                            query.AppendLine("ORDER BY format(CAST(a.CreatedDate AS DATE), 'MMM') DESC");
                            var orderCompletedsMonth = await dbConnection.QueryAsync<OrderCompleted>(query.ToString());
                            results.OrderCompletes = orderCompletedsMonth.ToList();
                            results.OrderCompleteSum = orderCompletedsMonth.Sum(x => x.OrderCompletedCount);
                            query = new StringBuilder();
                            query.AppendLine("SELECT a.RES_ID,FORMAT(CAST(a.CreatedDate AS DATE), 'MMM') AS Date, COUNT(ID) AS 'OrderPending',");
                            query.AppendLine("b.RestaurantNameEN,b.RestaurantNameJP");
                            query.AppendLine("from[Order] a inner join RESTAURANT b on a.RES_ID = b.RES_ID");
                            if (dateTime == null)
                            {
                                query.AppendLine("where a.CreatedDate > convert(date, getdate()) and a.[Status] < 20 and a.[Status] != 9  and a.RES_ID not in (250,83)");
                            }
                            else
                            {
                                query.AppendLine("where a.CreatedDate > convert(date, '" + dateTime + "',103) and a.[Status] < 20 and a.[Status] != 9  and a.RES_ID not in (250,83)");
                            }
                            query.AppendLine("AND format(CAST(a.CreatedDate AS DATE),'yyy') = format(CAST(GETDATE() AS DATE), 'yyy')");
                            query.AppendLine("GROUP BY a.RES_ID,format(CAST(a.CreatedDate AS DATE), 'MMM'),b.RestaurantNameEN,b.RestaurantNameJP");
                            query.AppendLine("ORDER BY format(CAST(a.CreatedDate AS DATE), 'MMM') DESC");
                            var orderPendingsMonth = await dbConnection.QueryAsync<OrderPending>(query.ToString());
                            results.OrderPendings = orderPendingsMonth.ToList();
                            results.OrderPendingSum = orderPendingsMonth.Sum(x => x.OrderPendingCount);
                            break;

                        default:
                            query.AppendLine("SELECT FORMAT(CAST(A.CreatedDate AS DATE), 'd MMM yyy') AS Date,a.RES_ID,b.RestaurantNameEN,b.RestaurantNameJP,count(ID) as 'OrderCurrentCount' from[Order] a ");
                            query.AppendLine("INNER JOIN RESTAURANT b ON a.RES_ID = b.RES_ID");
                            query.AppendLine("WHERE a.CreatedDate > convert(date, getdate()) and a.RES_ID not in (250, 83)");
                            query.AppendLine("group by a.RES_ID,b.RestaurantNameEN,b.RestaurantNameJP,FORMAT(CAST(a.CreatedDate AS DATE), 'd MMM yyy')");
                            var orderCurrents1 = await dbConnection.QueryAsync<OrderCurrent>(query.ToString());
                            results.OrderCurrents = orderCurrents1.ToList();
                            results.OrderCurrentSum = orderCurrents1.Sum(x => x.OrderCurrentCount);
                            query = new StringBuilder();
                            query.AppendLine("select FORMAT(CAST(GETDATE() AS DATE), 'd MMM yyy') AS Date,a.RES_ID,b.RestaurantNameEN,b.RestaurantNameJP,count(ID) as 'OrderCancelledCount' from[Order] a ");
                            query.AppendLine("inner join RESTAURANT b on a.RES_ID = b.RES_ID");
                            query.AppendLine("where a.CreatedDate > convert(date, getdate()) and a.[Status] in (98,99) and a.RES_ID not in (250,83)");
                            query.AppendLine("group by a.RES_ID,b.RestaurantNameEN,b.RestaurantNameJP,FORMAT(CAST(a.CreatedDate AS DATE), 'd MMM yyy')");
                            var orderCancel1 = await dbConnection.QueryAsync<OrderCancel>(query.ToString());
                            results.OrderCancels = orderCancel1.ToList();
                            results.OrderCancelSum = orderCancel1.Sum(x => x.OrderCancelledCount);
                            query = new StringBuilder();
                            query.AppendLine("select FORMAT(CAST(GETDATE() AS DATE), 'd MMM yyy') AS Date,a.RES_ID,b.RestaurantNameEN,b.RestaurantNameJP,count(ID) as 'OrderCompletedCount' from[Order] a ");
                            query.AppendLine("inner join RESTAURANT b on a.RES_ID = b.RES_ID");
                            query.AppendLine("where a.CreatedDate > convert(date,getdate()) and a.[Status] in (20,9) and a.RES_ID not in (250,83)");
                            query.AppendLine("group by a.RES_ID,b.RestaurantNameEN,b.RestaurantNameJP,FORMAT(CAST(a.CreatedDate AS DATE), 'd MMM yyy') ");
                            var orderCompleteds1 = await dbConnection.QueryAsync<OrderCompleted>(query.ToString());
                            results.OrderCompletes = orderCompleteds1.ToList();
                            results.OrderCompleteSum = orderCompleteds1.Sum(x => x.OrderCompletedCount);
                            query = new StringBuilder();
                            query.AppendLine("select FORMAT(CAST(GETDATE() AS DATE), 'd MMM yyy') AS Date,a.RES_ID,b.RestaurantNameEN,b.RestaurantNameJP,count(ID) as 'OrderPendingCount' from[Order] a ");
                            query.AppendLine("inner join RESTAURANT b on a.RES_ID = b.RES_ID");
                            query.AppendLine("where a.CreatedDate > convert(date, getdate()) and a.[Status] < 20 and a.[Status] != 9  and a.RES_ID not in (250,83)");
                            query.AppendLine("group by a.RES_ID,b.RestaurantNameEN,b.RestaurantNameJP,FORMAT(CAST(a.CreatedDate AS DATE), 'd MMM yyy')");
                            var orderPendings1 = await dbConnection.QueryAsync<OrderPending>(query.ToString());
                            results.OrderPendings = orderPendings1.ToList();
                            results.OrderPendingSum = orderPendings1.Sum(x => x.OrderPendingCount);

                            break;
                    }
                    return results;
                }

            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        public async Task<List<MenuExcel>> GetMenuExcel(int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine(" select RES_ID, A.[Sequence],A.Name_EN as 'NameMenuEN',A.[Name_JP] as 'NameMenuJP',A.Dec_EN as 'Deteil_MenuEN',A.Dec_JP as 'Deteil_MenuJP',B.Name as 'Name_Category',C.Name_EN as 'Name_ItemEN',C.Name_JP as 'Name_ItemJP',");
                    query.AppendLine("(select value from fn_split_string_to_column([DayOfWeek],',') a where column_id in (1)) as IsSun,");
                    query.AppendLine("(select value from fn_split_string_to_column([DayOfWeek],',') a where column_id in (2)) as IsMon,");
                    query.AppendLine("(select value from fn_split_string_to_column([DayOfWeek],',') a where column_id in (3)) as IsTue,");
                    query.AppendLine("(select value from fn_split_string_to_column([DayOfWeek],',') a where column_id in (4)) as IsWed,");
                    query.AppendLine("(select value from fn_split_string_to_column([DayOfWeek],',') a where column_id in (5)) as IsThu,");
                    query.AppendLine("(select value from fn_split_string_to_column([DayOfWeek],',') a where column_id in (6)) as IsFri,");
                    query.AppendLine("(select value from fn_split_string_to_column([DayOfWeek],',') a where column_id in (7)) as IsSat,");
                    query.AppendLine("convert(varchar,[StartTime], 8) as StartTime,convert(varchar,[EndTime], 8) as EndTime,");
                    query.AppendLine("convert(varchar,[StartTime2], 8) as StartTime2,convert(varchar,[EndTime2], 8) as EndTime2,");
                    query.AppendLine("convert(varchar,[StartTime3], 8) as StartTime3,convert(varchar,[EndTime3], 8) as EndTime3,");
                    query.AppendLine("convert(varchar,[StartTime4], 8) as StartTime4,convert(varchar,[EndTime4], 8) as EndTime4,");
                    query.AppendLine("convert(varchar,[StartTime5], 8) as StartTime5,convert(varchar,[EndTime5], 8) as EndTime5,");
                    query.AppendLine("convert(varchar,[StartTime6], 8) as StartTime6,convert(varchar,[EndTime6], 8) as EndTime6,");
                    query.AppendLine("convert(varchar,[StartTime7], 8) as StartTime7,convert(varchar,[EndTime7], 8) as EndTime7,");
                    query.AppendLine("C.Delivery,C.Pickup,C.DineIn,");
                    query.AppendLine("A.[CreatedDate] from MenuHour as A INNER JOIN MenuGroup as B ON B.MenuHour_ID = A.ID INNER JOIN MenuItem as C ON C.MenuGroup_ID = B.ID");
                    query.AppendLine("where RES_ID = 2 and A.IsEnable = 1");
                    query.AppendLine("ORDER BY A.Sequence");
                    var results = await dbConnection.QueryAsync<MenuExcel>(query.ToString(), new { RES_ID = resid });
                    return results.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<CustomerDailyReportAppsFlyerIOS>> GetLaCarteCustomerDailyReportAppsFlyerIOS(string results)
        {
            try
            {

                //StringBuilder sb = new StringBuilder();
                //using (var p = ChoCSVReader.LoadText(results)
                //    .WithFirstLineHeader()
                //    )
                //{
                //    using (var w = new ChoJSONWriter(sb))
                //        w.Write(p);
                //}

                //return sb.ToString().Replace("\r\n", "").Replace(@"\\", "").Replace(@"\", "").Replace("//", "").Replace("/", "").Replace(" ", "").Replace(@"\""","");


                StringBuilder sb = new StringBuilder();
                using (var p = ChoCSVReader.LoadText(results)
                    .WithField("Date", position: 1)
                    .WithField("Agency_PMD", position: 2)
                    .WithField("Media_Source", position: 3)
                    .WithField("Campaign", position: 4)
                    .WithField("Impressions", position: 5)
                    .WithField("Clicks", position: 6)
                    .WithField("CTR", position: 7)
                    .WithField("Installs", position: 8)
                    .WithField("Conversion_Rate", position: 9)
                    .WithField("Sessions", position: 10)
                    .WithField("Loyal_Users", position: 11)
                    .WithField("Loyal_Users_Installs", position: 12)
                    .WithField("Total_Cost", position: 13)
                    .WithField("Average_eCPI", position: 14)
                    .WithFirstLineHeader(true)
                    )
                {
                    using (var w = new ChoJSONWriter(sb))
                        w.Write(p);
                }


                return JsonConvert.DeserializeObject<List<CustomerDailyReportAppsFlyerIOS>>(sb.ToString());
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }


        public async Task<List<RestaurantDailyReportAppsFlyerIOS>> GetLaCarteRestaurantDailyReportAppsFlyerIOS(string results)
        {
            try
            {


                //StringBuilder sb = new StringBuilder();
                //using (var p = ChoCSVReader.LoadText(results)
                //    .WithFirstLineHeader()
                //    )
                //{
                //    using (var w = new ChoJSONWriter(sb))
                //        w.Write(p);
                //}


                //return sb.ToString().Replace("\r\n", "").Replace(@"\\", "").Replace(@"\", "").Replace("//", "").Replace("/", "").Replace(" ", "").Replace(@"\""", "");

                StringBuilder sb = new StringBuilder();
                using (var p = ChoCSVReader.LoadText(results)
                    .WithField("Date", position: 1)
                    .WithField("Agency_PMD", position: 2)
                    .WithField("Media_Source", position: 3)
                    .WithField("Campaign", position: 4)
                    .WithField("Impressions", position: 5)
                    .WithField("Clicks", position: 6)
                    .WithField("CTR", position: 7)
                    .WithField("Installs", position: 8)
                    .WithField("Conversion_Rate", position: 9)
                    .WithField("Sessions", position: 10)
                    .WithField("Loyal_Users", position: 11)
                    .WithField("Loyal_Users_Installs", position: 12)
                    .WithField("Total_Cost", position: 13)
                    .WithField("Average_eCPI", position: 14)
                    .WithFirstLineHeader(true)
                    )
                {
                    using (var w = new ChoJSONWriter(sb))
                        w.Write(p);
                }


                return JsonConvert.DeserializeObject<List<RestaurantDailyReportAppsFlyerIOS>>(sb.ToString());
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
    }
}

