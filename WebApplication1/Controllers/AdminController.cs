﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices.ComTypes;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using DocumentFormat.OpenXml;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Net.Http.Headers;
using Nancy.Json;
using Stripe;
using WebApplication1.Models;
using WebApplication1.Repository;
using static WebApplication1.Models.Admin;
using Order = WebApplication1.Models.Order;
using StatusCodes = WebApplication1.Models.StatusCodes;

namespace WebApplication1.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AdminController : ControllerBase
    {
        private readonly IAdminData _adminData;
        private readonly IConfiguration _configuration;

        public AdminController(IAdminData adminData, IConfiguration configuration)
        {
            this._adminData = adminData;
            this._configuration = configuration;
        }

        #region BuildToken Generator
        private Admin.UserToken BuildToken(AdminInfo data, string guid)
        {
            var claims = new List<Claim>()
            {
                new Claim(ClaimTypes.Sid, guid),
                new Claim(ClaimTypes.Email, data.Email),
                new Claim(ClaimTypes.MobilePhone, data.PhoneNumber ?? ""),
                new Claim(ClaimTypes.NameIdentifier, data.Admin_ID.ToString()),
                new Claim(ClaimTypes.Role, data.Application.ToString()),
                new Claim("mykey",_configuration["JWT:mykeyAdmin"])
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var expiration = DateTime.UtcNow.AddYears(1);

            JwtSecurityToken token = new JwtSecurityToken(
                issuer: null,
                audience: null,
                claims: claims,
                expires: expiration,
                signingCredentials: creds);
            return new Admin.UserToken()
            {
                Token = new JwtSecurityTokenHandler().WriteToken(token),
                Expiration = expiration
            };
        }
        #endregion

        private async Task<string> SendNotiOrderCustomer(int orderid, int resid)
        {
            RestaurantAllData _restaurant;
            OrderDetail _order;
            List<Notification> _listNotiDevice;
            string titleEN = "";
            string titleJP = "";
            var messageEN = new StringBuilder();
            var messageJP = new StringBuilder();

            var request = WebRequest.Create("https://onesignal.com/api/v1/notifications") as HttpWebRequest;

            request.KeepAlive = true;
            request.Method = "POST";
            request.ContentType = "application/json; charset=utf-8";
            request.Headers.Add("authorization", "Basic " + _configuration["onesignal:apikeyCustomer"]);

            try
            {
                _restaurant = await _adminData.GetRestaurantByID(resid);
                _order = await _adminData.GetOrderByID(orderid, resid);
                _listNotiDevice = await _adminData.GetNotiDeviceCustomer(_order.CUS_ID);
                _listNotiDevice = _listNotiDevice.Where(x => x.Device_ID != null).ToList();
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.AppendLine(" Restaurant SendNotiOrderCustomer1");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return ex.Message;
            }
            switch (_order.Status)
            {
                case OrderStatus.Cooking:
                    messageEN = new StringBuilder();
                    titleEN = "Order " + _order.Order_No + " : Accepted";
                    titleJP = "Order " + _order.Order_No + " : Accepted";
                    messageEN.Append((_restaurant.RestaurantNameEN ??= _restaurant.RestaurantNameJP) + " has accepted your order. Your order will be ready soon!");
                    messageJP.Append(_restaurant.RestaurantNameJP + " がお客様のご注文を受け付けました。");
                    break;
                case OrderStatus.Cooked:
                    titleEN = "Order " + _order.Order_No + " : Done Cooking";
                    titleJP = "Order " + _order.Order_No + " : Done Cooking";
                    messageEN.Append("Your order from " + (_restaurant.RestaurantNameEN ??= _restaurant.RestaurantNameJP) + " is done cooking and ready to go!");
                    messageJP.Append("ご注文の準備ができました！");
                    break;
                case OrderStatus.Ready:
                    if (_order.Order_Type == OrderType.Delivery)
                    {
                        titleEN = "Order " + _order.Order_No + " : On The Way";
                        titleJP = "Order " + _order.Order_No + " : On The Way";
                        messageEN.Append((_restaurant.RestaurantNameEN ??= _restaurant.RestaurantNameJP) + "’s delivery agent has picked up your order and is on the way to your location.");
                        messageJP.Append("配達スタッフが注文をxから受け取りました。");
                    }
                    else if (_order.Order_Type == OrderType.Pickup)
                    {
                        titleEN = "Order " + _order.Order_No + " : Ready";
                        titleJP = "Order " + _order.Order_No + " : Ready";
                        messageEN.Append("Your take-out order from " + (_restaurant.RestaurantNameEN ??= _restaurant.RestaurantNameJP) + " is ready for pickup. Please present your order number when you arrive.");
                        messageJP.Append(_restaurant.RestaurantNameJP + " のテイクアウト注文の準備ができました。ご来店の際、店員にオーダー番号をご提示ください。");
                    }
                    break;
                case OrderStatus.Complete:
                    titleEN = "Order " + _order.Order_No + " : Completed";
                    titleJP = "Order " + _order.Order_No + " : Completed";
                    messageEN.Append("Your order from " + (_restaurant.RestaurantNameEN ??= _restaurant.RestaurantNameJP) + " has been successfully completed.");
                    messageJP.Append(_restaurant.RestaurantNameJP + " の注文が完了しました。");
                    break;
                case OrderStatus.RestaurantCancel:
                    titleEN = "Order " + _order.Order_No + " : Canceled";
                    titleJP = "Order " + _order.Order_No + " : Canceled";
                    messageEN.Append("Your order from " + (_restaurant.RestaurantNameEN ??= _restaurant.RestaurantNameJP) + " has been cancelled by the restaurant. Please contact " + _restaurant.OfficialPhone + " for more information. We apologize for the inconvenience. ");
                    messageJP.Append("Your order from " + _restaurant.RestaurantNameJP + " has been cancelled by the restaurant.Please contact " + _restaurant.OfficialPhone + " for more information. We apologize for the inconvenience. ");
                    break;
            }

            var serializer = new JavaScriptSerializer();
            var obj = new
            {
                app_id = _configuration["onesignal:appidCustomer"],
                headings = new { en = titleEN, ja = titleJP },
                contents = new { en = messageEN.ToString(), ja = messageJP.ToString() },
                data = new { orderId = orderid },
                include_player_ids = _listNotiDevice.Where(x => x.IsEnable == true).Select(x => x.Device_ID).ToList()
            };

            var param = serializer.Serialize(obj);
            byte[] byteArray = Encoding.UTF8.GetBytes(param);

            string responseContent = null;

            try
            {
                using (var writer = request.GetRequestStream())
                {
                    writer.Write(byteArray, 0, byteArray.Length);
                }

                using (var response = request.GetResponse() as HttpWebResponse)
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        responseContent = reader.ReadToEnd();
                    }
                }
            }
            catch (WebException ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.AppendLine(" Restaurant SendNotiOrderCustomer2");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return ex.Message;
            }

            return responseContent;
        }

        [AllowAnonymous]
        [HttpPost("Login")]
        public async Task<ActionResult<ResultLoginAdmin>> GetLogin(LoginAdmin data)
        {
            ResultLoginAdmin resdata = new ResultLoginAdmin();
            AdminInfo _data = new AdminInfo();
            string guid = Guid.NewGuid().ToString("n");
            bool logstatus = false;
            try
            {
                _data = await _adminData.GetLoginAdmin(data);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.AppendLine(" WebAdmin GetLogin");
                lineNoti.AppendLine("");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }
            if (_data == null)
                return Unauthorized("Username or Password Incorrect");

            try
            {
                logstatus = await _adminData.PostLogAdminLogin(_data, guid);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.AppendLine(" WebAdmin GetLogin");
                lineNoti.AppendLine("");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }
            if (logstatus == false)
            {
                resdata.StatusCode = (int)(StatusCodes.Error);
                resdata.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
            }
            else
            {
                resdata.StatusCode = (int)(StatusCodes.Succuss);
                resdata.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                if (_data.Application == "1,2")
                {
                    resdata.Results = BuildToken(_data, guid);
                }
                else
                {
                    resdata.Results = BuildToken(_data, guid);
                }
            }
            return resdata;
        }

        [AllowAnonymous]
        [HttpPost("SignUp")]
        public async Task<ActionResult<Result>> PostRegister(SignUpAdmin data)
        {
            Result resdata = new Result();
            bool status = false;
            try
            {
                status = await _adminData.PostRegister(data);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.AppendLine(" WebAdmin PostRegister");
                lineNoti.AppendLine("");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == false)
            {
                resdata.StatusCode = (int)(StatusCodes.Error);
                resdata.StatusMessages = (String)(EnumString.GetStringValue(StatusCodes.Error));
                resdata.Messages = "Email or Phone number is Deplitcate";
            }
            else
            {
                var logindata = new LoginAdmin();
                logindata.Email = data.Email;
                logindata.Password = data.Password;
                AdminInfo _data = new AdminInfo();
                bool logstatus = false;
                string guid = Guid.NewGuid().ToString("n");
                try
                {
                    _data = await _adminData.GetLoginAdmin(logindata);
                    logstatus = await _adminData.PostLogAdminLogin(_data, guid);
                }
                catch (Exception ex)
                {
                    var lineNoti = new StringBuilder();
                    lineNoti.Append(" WebAdmin PostRegister");
                    lineNoti.AppendLine(ex.Message);
                    SystemNotification.SendLineNotify(lineNoti.ToString());
                    return BadRequest(ex.Message);
                }
                if (logstatus == false)
                {
                    resdata.StatusCode = (int)(StatusCodes.Error);
                    resdata.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                }
                else
                {
                    resdata.StatusCode = (int)(StatusCodes.Succuss);
                    resdata.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                    resdata.Results = BuildToken(_data, guid);
                }

            }
            return resdata;
        }

        [HttpGet("GetAllRestaurant")]
        public async Task<ActionResult<Result>> GetAllRestaurant()
        {
            Result resData = new Result();
            List<RestaurantAllData> _data;
            try
            {
                _data = await _adminData.GetAllRestaurant();
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.AppendLine(" WebAdmin GetAllRestaurant");
                lineNoti.AppendLine("");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }
            if (_data == null)
            {
                resData.StatusCode = (int)StatusCodes.Error;
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Data Is Null";
            }
            else
            {
                resData.StatusCode = (int)StatusCodes.Succuss;
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "";
            }
            resData.Results = _data;
            return resData;
        }

        [HttpGet("restaurantByID")]
        public async Task<ActionResult<Result>> GetRestaurantByID(int RES_ID)
        {
            Result resData = new Result();
            RestaurantAllData restaurant;
            try
            {
                restaurant = await _adminData.GetRestaurantByID(RES_ID);
                if (restaurant != null)
                {
                    restaurant.LogoPath = restaurant.Logo != null ? _configuration["imagePath:restaurant"] + restaurant.Logo : null;
                    var listMenu = await _adminData.GetMenuCategoryByResID(restaurant.RES_ID);
                    if (listMenu.ToList().Count > 0)
                        restaurant.HasMenu = true;
                    else
                        restaurant.HasMenu = false;
                    if (restaurant.AccountID != null)
                    {
                        StripeConfiguration.ApiKey = _configuration["PaymentSettings:StripePrivateKey"];
                        var service = new AccountService();
                        Stripe.Account account = service.Get(restaurant.AccountID);
                        restaurant.StripeChargeEnable = account.ChargesEnabled;
                        restaurant.StripePayoutEnable = account.PayoutsEnabled;
                    }
                    else
                    {
                        restaurant.StripeChargeEnable = false;
                        restaurant.StripePayoutEnable = false;
                    }
                }
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.AppendLine(" WebAdmin GetRestaurantByID");
                lineNoti.AppendLine("");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (restaurant == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is null";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = restaurant;
            }
            return resData;
        }

        [HttpPost("menuhour")]
        public async Task<ActionResult<Result>> PostMenuHour(MenuHour data)
        {
            Result resData = new Result();
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int user_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _adminData.PostMenuHour(data);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.AppendLine(" WebAdmin PostMenuHour");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data invalid";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data saved";
            }
            return resData;
        }

        [HttpPut("menuhour")]
        public async Task<ActionResult<Result>> PutMenuHour(MenuHour data)
        {
            Result resData = new Result();
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _adminData.PutMenuHour(data);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.AppendLine(" WebAdmin PutMenuHour");
                lineNoti.AppendLine(" ");
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data invalid";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data updated";
            }
            return resData;
        }

        [HttpDelete("menuhour")]
        public async Task<ActionResult<Result>> DeleteMenuHour(int MenuHour_ID, int RES_ID)
        {
            Result resData = new Result();
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int user_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _adminData.DeleteMenuHour(MenuHour_ID, RES_ID);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.AppendLine(" WebAdmin DeleteMenuHour");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data invalid";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data deleted";
            }
            return resData;
        }

        [HttpPost("menugroup")]
        public async Task<ActionResult<Result>> PostMenuGroup(MenuGroup data)
        {
            Result resData = new Result();
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _adminData.PostMenuGroup(data);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.AppendLine(" WebAdmin PostMenuGroup");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data invalid";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data saved";
            }
            return resData;
        }

        [HttpPut("menugroup")]
        public async Task<ActionResult<Result>> PutMenuGroup(MenuGroup data)
        {
            Result resData = new Result();
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _adminData.PutMenuGroup(data);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.AppendLine(" WebAdmin PutMenuGroup");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data invalid";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data updated";
            }
            return resData;
        }

        [HttpDelete("menugroup")]
        public async Task<ActionResult<Result>> DeleteMenuGroup(int MenuGroup_ID)
        {
            Result resData = new Result();
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _adminData.DeleteMenuGroup(MenuGroup_ID);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.AppendLine(" WebAdmin DeleteMenuGroup");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data invalid";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data deleted";
            }
            return resData;
        }

        [HttpPut("PutRestaurantInfo")]
        public async Task<ActionResult<Result>> PutRestaurantInfo(ChangeRestaurantInfo data)
        {
            Result resData = new Result();
            int status = 0;
            try
            {
                status = await _adminData.PutRestaurantInfo(data);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.AppendLine(" WebAdmin PutRestaurantInfo");
                lineNoti.AppendLine("");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }
            if (status == 10)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data updated";
            }
            else if (status == 1)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "email or phone duplicate";
            }
            else if (status == 2)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "placeid duplicate";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data invalid";
            }
            return resData;
        }

        [HttpPut("putMenuItem")]
        public async Task<ActionResult<Result>> PutMenuItem(PutMenuItem data)
        {
            Result res_data = new Result();
            bool status = false;
            try
            {
                status = await _adminData.PutMenuItem(data);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
            if (status == false)
            {
                res_data.StatusCode = (int)(StatusCodes.Error);
                res_data.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                res_data.Messages = "Data InVaild";
            }
            else
            {
                res_data.StatusCode = (int)(StatusCodes.Succuss);
                res_data.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                res_data.Messages = "Data Update";
            }
            return res_data;
        }
        [HttpPut("putAddOn")]
        public async Task<ActionResult<Result>> PutAddOn(PutAddOn data)
        {
            Result res_data = new Result();
            bool status = false;
            try
            {
                status = await _adminData.PutAddOn(data);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
            if (status == false)
            {
                res_data.StatusCode = (int)(StatusCodes.Error);
                res_data.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                res_data.Messages = "Data InVaild";
            }
            else
            {
                res_data.StatusCode = (int)(StatusCodes.Succuss);
                res_data.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                res_data.Messages = "Data Update";
            }
            return res_data;
        }
        [HttpPut("putAddOnOption")]
        public async Task<ActionResult<Result>> PutAddOnOption(PutAddOnOption data)
        {
            Result res_data = new Result();
            bool status = false;
            try
            {
                status = await _adminData.PutAddOnOption(data);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
            if (status == false)
            {
                res_data.StatusCode = (int)(StatusCodes.Error);
                res_data.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                res_data.Messages = "Data IsVaild";
            }
            else
            {
                res_data.StatusCode = (int)(StatusCodes.Succuss);
                res_data.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                res_data.Messages = "Data Update";
            }
            return res_data;
        }


        [HttpGet("allMenuHours")]
        public async Task<ActionResult<Result>> GetAllMenuHour(int RES_ID)
        {
            Result resData = new Result();
            List<ViewMenuHour2> menuhours;

            try
            {
                menuhours = await _adminData.GetAllMenuHour(RES_ID);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.AppendLine(" WebAdmin GetAllMenuHour");
                lineNoti.AppendLine("");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (menuhours == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is null";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "";
            }
            resData.Results = menuhours;
            return resData;
        }

        [HttpGet("menuGroupByHourID")]
        public async Task<ActionResult<Result>> GetMenuGroupByHourID(int MenuHourID)
        {
            Result resData = new Result();
            List<MenuGroup> menugroup;

            try
            {
                menugroup = await _adminData.GetMenuGroupByHourID(MenuHourID);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.AppendLine(" WebAdmin GetMenuGroupByHourID");
                lineNoti.AppendLine("");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message.ToString());
            }

            if (menugroup == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is null";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "";
            }
            resData.Results = menugroup;
            return resData;
        }

        [HttpPut("dumpMenuItem")]
        public async Task<ActionResult<Result>> PutDumpMenuItem(DumpMenuItem data)
        {
            Result resData = new Result();
            bool status = false;
            List<ExcelMenuItem> menuItems;
            ConfirmDumpExcel confirmStatus = new ConfirmDumpExcel() { RES_ID = data.RES_ID, Status = true };
            try
            {
                status = await _adminData.PutDumpMenuItem(data);
                menuItems = await _adminData.GetDumpMenuItemByResID(data.RES_ID);
                if (status == false || menuItems == null)
                {
                    confirmStatus.Status = false;
                    resData.StatusCode = (int)(StatusCodes.Error);
                    resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                    resData.Messages = "data invalid";
                    return resData;
                }
                status = await _adminData.PutConfirmDumpMenuItem(confirmStatus);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.AppendLine(" WebAdmin PutDumpMenuItem");
                lineNoti.AppendLine("");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }
            if (status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data invalid";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "";
            }
            resData.Results = menuItems;
            return resData;
        }

        [HttpPut("dumpAddon")]
        public async Task<ActionResult<Result>> PutDumpAddon(DumpAddon data)
        {
            Result resData = new Result();
            bool status = false;
            List<ViewExcelAddon> listAddons;
            ConfirmDumpExcel confirmStatus = new ConfirmDumpExcel() { RES_ID = data.RES_ID, Status = true };
            try
            {
                status = await _adminData.PutDumpAddon(data);
                listAddons = await _adminData.GetDumpAddonByResID(data.RES_ID);
                var listError = listAddons.Where(x => x.MenuItemName_JP == null).ToList();
                if (status == false || listAddons == null || listError.Count() > 0)
                {
                    confirmStatus.Status = false;
                    resData.StatusCode = (int)(StatusCodes.Error);
                    resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                    resData.Messages = "data invalid";
                    resData.Results = listError;
                    return resData;
                }
                status = await _adminData.PutConfirmDumpAddon(confirmStatus);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.AppendLine(" WebAdmin PutDumpAddon");
                lineNoti.AppendLine("");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }
            if (status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data invalid";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "";
            }
            resData.Results = listAddons;
            return resData;
        }

        [HttpPut("dumpAddonOption")]
        public async Task<ActionResult<Result>> PutDumpAddonOption(DumpAddonOption data)
        {
            Result resData = new Result();
            bool status = false;
            List<ViewExcelAddonOption> listAddonOptions;
            ConfirmDumpExcel confirmStatus = new ConfirmDumpExcel() { RES_ID = data.RES_ID, Status = true };
            try
            {
                status = await _adminData.PutDumpAddonOption(data);
                listAddonOptions = await _adminData.GetDumpAddonOptionByResID(data.RES_ID);
                var listError = listAddonOptions.Where(x => x.AddonName_JP == null).ToList();
                if (status == false || listAddonOptions == null || listError.Count() > 0)
                {
                    confirmStatus.Status = false;
                    resData.StatusCode = (int)(StatusCodes.Error);
                    resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                    resData.Messages = "data invalid";
                    resData.Results = listError;
                    return resData;
                }
                status = await _adminData.PutConfirmDumpAddonOption(confirmStatus);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.AppendLine(" WebAdmin PutDumpAddonOption");
                lineNoti.AppendLine("");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }
            if (status == false || listAddonOptions == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data invalid";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "";
            }
            resData.Results = listAddonOptions;
            return resData;
        }

        [HttpGet("allOrder")]
        public async Task<ActionResult<Result>> GetAllOrder(int? RES_ID, OrderStatus? Status)
        {
            Result resData = new Result();
            List<Order> listResult;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            try
            {
                listResult = await _adminData.GetAllOrder(RES_ID, Status);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" WebAdmin GetAllOrder");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message.ToString());
            }

            if (listResult.Count == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is null";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "";
            }
            resData.Results = listResult;
            return resData;
        }

        [HttpGet("orderByID")]
        public async Task<ActionResult<Result>> GetOrderByID(int ORDER_ID, int RES_ID)
        {
            Result resData = new Result();
            OrderDetail _data;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            try
            {
                _data = await _adminData.GetOrderByID(ORDER_ID, RES_ID);
                if (_data.Agent != null)
                    _data.Agent.ImagePath = _data.Agent.ImageName != null ? _configuration["imagePath:agent"] + _data.Agent.ImageName.Replace(@"\", "/") : null;
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" WebAdmin GetOrderByID");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (_data == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is null";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "";
            }
            resData.Results = _data;
            return resData;
        }

        [HttpPut("cancelOrder")]
        public async Task<ActionResult<Result>> PutCancelOrder(ChangeStatusOrder data)
        {
            Result resData = new Result();
            OrderDetail _dataOrder;
            RestaurantAllData _dataRestaurant;
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            try
            {
                _dataOrder = await _adminData.GetOrderByID(data.Order_ID, data.RES_ID);
                _dataRestaurant = await _adminData.GetRestaurantByID(_dataOrder.RES_ID);
                if (_dataOrder.PaymentMethod == OrderPayment.Credit)
                {
                    var options = new RefundCreateOptions
                    {
                        PaymentIntent = _dataOrder.Payment_ID,
                    };

                    var requestOptions = new RequestOptions
                    {
                        StripeAccount = _dataRestaurant.AccountID,
                    };

                    var service = new RefundService();
                    service.Create(options, requestOptions);
                }
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" WebAdmin PutCancelOrder");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            try
            {
                status = await _adminData.PutCancelOrder(data);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" WebAdmin PutCancelOrder");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data invalid";
            }
            else
            {
                await SendNotiOrderCustomer(data.Order_ID, data.RES_ID);
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data updated";
            }
            return resData;
        }
        [HttpGet("getOrderHistory")]
        public async Task<ActionResult<Result>> GetOrderHistoryByType(string dateTime, OrderHistoryType orderHistoryType)
        {
            Result result = new Result();
            OrderHistory orderHistory;
            try
            {
                orderHistory = await _adminData.GetOrderHistoryType(dateTime, orderHistoryType);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
            if (orderHistory == null)
            {
                result.StatusCode = (int)(StatusCodes.Error);
                result.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
            }
            else
            {
                result.StatusCode = (int)(StatusCodes.Succuss);
                result.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
            }
            result.Results = orderHistory;
            return result;
        }
        [HttpGet("menuExcel")]
        public async Task<ActionResult<Result>> GetMenuExcel(int RES_ID)
        {
            Result resData = new Result();
            List<MenuExcel> menuhours;

            try
            {
                menuhours = await _adminData.GetMenuExcel(RES_ID);
            }
            catch (Exception ex)
            {
               /* var lineNoti = new StringBuilder();
                lineNoti.AppendLine(" WebAdmin GetMenuExcel");
                lineNoti.AppendLine("");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());*/
                return BadRequest(ex.Message);
            }

            if (menuhours == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is null";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "";
            }
            resData.Results = menuhours;
            return resData;
        }

        [HttpGet("DownloadZip")]
        public async Task<ActionResult<Result>> DownloadZipFile()
        {
            string path = @"D:\fileName.zip";
            Result res_data = new Result();

            var response = new HttpResponseMessage(HttpStatusCode.OK);
            var stream = new System.IO.FileStream(path, System.IO.FileMode.Open);
            response.Content = new StreamContent(stream);
            response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/octet-stream");
            res_data.Results = response;
            return res_data;
        }


        [HttpGet("GetLaCarteCustomerDailyReportAppsFlyerIOS")]
        public async Task<ActionResult<Result>> GetLaCarteCustomerDailyReportAppsFlyerIOS(string start_date, string end_date)
        {
            Result resData = new Result();

            var _url = _configuration["appsflyer:customerDaily"] ;
            var _urlWithFromAndToDate = _url + "&from=" + start_date + "&to=" + end_date;

            //string _data;
            List<CustomerDailyReportAppsFlyerIOS> _data;
            try
            {
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(_urlWithFromAndToDate);
                HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

                StreamReader sr = new StreamReader(resp.GetResponseStream());
                string results  = sr.ReadToEnd();
                sr.Close();

                _data = await _adminData.GetLaCarteCustomerDailyReportAppsFlyerIOS(results);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.AppendLine(" WebAdmin GetLaCarteCustomerDailyReportAppsFlyerIOS");
                lineNoti.AppendLine("");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }
            if (_data == null)
            {
                resData.StatusCode = (int)StatusCodes.Error;
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Data Is Null";
            }
            else
            {
                resData.StatusCode = (int)StatusCodes.Succuss;
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "";
            }
            resData.Results = _data;
            return resData;
        }


        [HttpGet("GetLaCarteRestaurantDailyReportAppsFlyerIOS")]
        public async Task<ActionResult<Result>> GetLaCarteRestaurantDailyReportAppsFlyerIOS(string start_date, string end_date)
        {
            Result resData = new Result();

            var _url = _configuration["appsflyer:restaurantDaily"];
            var _urlWithFromAndToDate = _url + "&from=" + start_date + "&to=" + end_date;

            //string _data;
            List<RestaurantDailyReportAppsFlyerIOS> _data;
            try
            {
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(_urlWithFromAndToDate);
                HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

                StreamReader sr = new StreamReader(resp.GetResponseStream());
                string results = sr.ReadToEnd();
                sr.Close();

                _data = await _adminData.GetLaCarteRestaurantDailyReportAppsFlyerIOS(results);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.AppendLine(" WebAdmin GetLaCarteRestaurantDailyReportAppsFlyerIOS");
                lineNoti.AppendLine("");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }
            if (_data == null)
            {
                resData.StatusCode = (int)StatusCodes.Error;
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Data Is Null";
            }
            else
            {
                resData.StatusCode = (int)StatusCodes.Succuss;
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "";
            }
            resData.Results = _data;
            return resData;
        }
    }
}
