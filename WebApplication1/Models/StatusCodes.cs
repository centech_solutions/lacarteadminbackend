﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class Result
    {
        public int StatusCode { get; set; }
        public string StatusMessages { get; set; }
        public string Messages { get; set; }
        public object Results { get; set; }
    }

    public class ResultLoginCustomer
    {
        public int StatusCode { get; set; }
        public string StatusMessages { get; set; }
        public string Messages { get; set; }
        public object Results { get; set; }
    }

    public class ResultLoginAdmin
    {
        public int StatusCode { get; set; }
        public string StatusMessages { get; set; }
        public string Messages { get; set; }
        public object Results { get; set; }

    }
    public enum StatusCodes
    {
        [EnumValue("Connection Error.")]
        Connection = 99,
        [EnumValue("Success.")]
        Succuss = 1,
        [EnumValue("Error.")]
        Error = 0
    }
}
