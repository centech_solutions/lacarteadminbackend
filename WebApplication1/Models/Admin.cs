﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class Admin
    {
        public int Admin_ID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Email
        {
            get; set;
        }
        public class AdminInfo
        {
            public int Admin_ID { get; set; }
            public string Firstname { get; set; }
            public string Lastname { get; set; }
            public string Email { get; set; }
            public string Token { get; set; }
            public string PhoneNumber { get; set; }
            public string Password { get; set; }
            public string Image { get; set; }
            public string ImagePath { get; set; }
            public string StripeID { get; set; }
            public bool IsGuest { get { return Token != null; } }
            public string Application { get; set; }
            public AppLanguageSelect AppLanguage { get; set; }
        }

        public class LoginAdmin
        {
            public string Email { get; set; }
            public string Password { get; set; }
            public AppFlag Application { get; set; }
        }
 

        public class SignUpAdmin
        {
            public string Firstname { get; set; }
            public string Lastname { get; set; }
            public string Email { get; set; }
            public string PhoneNumber { get; set; }
            public string Password { get; set; }
        }
        public class LogAdminLogin
        {
            public int Admin_ID { get; set; }
            public string CreatedDate { get; set; }
            public string IpAddress { get; set; }
            public string ApplicationName { get; set; }
        }

        public class UserToken
        {
            public string Token { get; set; }
            public DateTime Expiration { get; set; }
        }
    }
}
