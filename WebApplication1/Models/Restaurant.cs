﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class CheckRestaurantStatus
    {
        public string RestaurantStatus { get; set; }
    }

    public class RestaurantInfo
    {
        public int RES_ID { get; set; }
        public string RestaurantNameJP { get; set; }
        public string RestaurantNameEN { get; set; }
        public string Logo { get; set; }
        public string LogoPath { get; internal set; }
        public string Location { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Postcode { get; set; }
        public bool? ConfirmAddress { internal get ; set; }
        public string Email { get; set; }
        public string OfficialPhone { get; set; }
        public bool? ConfirmOfficialPhone { get; set; }
        public string PhoneNumber { get; set; }
        public decimal? Rate { get; set; }
        public int? PriceRate { get; set; }
        public int? Cuisine_ID { get; set; }
        public Cuisine cuisine { get; set; }
        public string CuisineOtherName { get; set; }
        public RestaurantService RestaurantService { get; set; }
        public DeliveryFee DeliveryFee { get; set; }
        public int? Status { get; set; }
        public string PlaceId { get; set; }
        public string AccountID { get; set; }
        public bool? StripeChargeEnable { get; internal set; }
        public bool? StripePayoutEnable { get; internal set; }
        public bool HasMenu { get; internal set; }
        public AppLanguageSelect AppLanguage { get; set; }
    }

    public class RestaurantAllData
    {
        public int RES_ID { get; set; }
        public string RestaurantNameJP { get; set; }
        public string RestaurantNameEN { get; set; }
        public string Logo { get; set; }
        public string LogoPath { get; internal set; }
        public string Location { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Postcode { get; set; }
        public bool? ConfirmAddress { internal get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string OfficialPhone { get; set; }
        public bool? ConfirmOfficialPhone { get; set; }
        public string PhoneNumber { get; set; }
        public decimal? Rate { get; set; }
        public int? PriceRate { get; set; }
        public int? Cuisine_ID { get; set; }
        public Cuisine cuisine { get; set; }
        public string CuisineOtherName { get; set; }
        public RestaurantService RestaurantService { get; set; }
        public DeliveryFee DeliveryFee { get; set; }
        public int? Status { get; set; }
        public string PlaceId { get; set; }
        public string AccountID { get; set; }
        public bool? StripeChargeEnable { get; internal set; }
        public bool? StripePayoutEnable { get; internal set; }
        public bool HasMenu { get; internal set; }
        public AppLanguageSelect AppLanguage { get; set; }
    }

    public class ChangeRestaurantInfo
    {
        public int RES_ID { get; set; }
        public string RestaurantNameJP { get; set; }
        public string RestaurantNameEN { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Postcode { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
    }
    public class Cuisine
    {
        public int ID { get; set; }
        public string NameEN { get; set; }
        public string NameJP { get; set; }
        public bool IsEnable { get; set; }
    }
    public class RestaurantService
    {
        public bool IsPickup { get; set; }
        public bool IsDelivery { get; set; }
        public bool IsDineIn { get; set; }
    }
    public class DeliveryFee
    {
        public decimal? KM1_Max { get; set; }
        public decimal? KM1_Price { get; set; }
        public decimal? KM2_Max { get; set; }
        public decimal? KM2_Price { get; set; }
        public decimal? KM3_Max { get; set; }
        public decimal? KM3_Price { get; set; }
    }

    public class RestaurantDailyReportAppsFlyerIOS
    {
        public string Date { get; set; }
        public string Agency_PMD { get; set; }
        public string Media_Source { get; set; }
        public string Campaign { get; set; }
        public string Impressions { get; set; }
        public string Clicks { get; set; }
        public string CTR { get; set; }
        public int Installs { get; set; }
        public string Conversion_Rate { get; set; }
        public int Sessions { get; set; }
        public int Loyal_Users { get; set; }
        public string Loyal_Users_Installs { get; set; }
        public string Total_Cost { get; set; }
        public string Average_eCPI { get; set; }

    }
}
