﻿using System;
using System.Collections.Generic;

namespace WebApplication1.Models
{
    public partial class Customer
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }
    public class CustomerInfo
    {
        public int CUS_ID { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Token { get; set; }
        public string PhoneNumber { get; set; }
        public string Password { get; set; }
        public string Image { get; set; }
        public string ImagePath { get; set; }
        public string StripeID { get; set; }
        public bool IsGuest { get { return Token != null; } }
        public AppLanguageSelect AppLanguage { get; set; }
    }

    public class LoginCustomer
    {
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Password { get; set; }
    }

    public class LogCustomerLogin
    {
        public int CUS_ID { get; set; }
        public string CreatedDate { get; set; }
        public string IpAddress { get; set; }
        public string ApplicationName { get; set; }
    }

    public class UserToken
    {
        public string Token { get; set; }
        public DateTime Expiration { get; set; }
    }

    public class CustomerDailyReportAppsFlyerIOS
    {
        public string Date { get; set; }
        public string Agency_PMD { get; set; }
        public string Media_Source { get; set; }
        public string Campaign { get; set; }
        public string Impressions { get; set; }
        public string Clicks { get; set; }
        public string CTR { get; set; }
        public int Installs { get; set; }
        public string Conversion_Rate { get; set; }
        public int Sessions { get; set; }
        public int Loyal_Users { get; set; }
        public string Loyal_Users_Installs { get; set; }
        public string Total_Cost { get; set; }
        public string Average_eCPI { get; set; }

    }

}
