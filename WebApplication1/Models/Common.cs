﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class EnumValue : System.Attribute
    {
        private string _value;
        public EnumValue(string value)
        {
            _value = value;
        }
        public string Value
        {
            get { return _value; }
        }
    }

    public static class EnumString
    {
        public static string GetStringValue(Enum value)
        {
            string output = null;
            Type type = value.GetType();
            FieldInfo fi = type.GetField(value.ToString());
            EnumValue[] attrs = fi.GetCustomAttributes(typeof(EnumValue), false) as EnumValue[];
            if (attrs.Length > 0)
            {
                output = attrs[0].Value;
            }
            return output;
        }
    }

    public static class EnumExtension
    {
        public static string ToDescription(this System.Enum value)
        {
            try
            {
                var attributes = (DescriptionAttribute[])value.GetType().GetField(value.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false);
                return attributes.Length > 0 ? attributes[0].Description : value.ToString();
            }
            catch (Exception)
            {
                return "";
            }
        }
    }


    public static class CalculateLocation
    {
        public static Location GetLocation(string Location)
        {
            var coordArrey = Location.Split(',');
            var location = new Location();
            location.Latitude = double.Parse(coordArrey[0]);
            location.Longitude = double.Parse(coordArrey[1]);
            return location;
        }

        public static double CalculateDistance(Location point1, Location point2)
        {
            var d1 = point1.Latitude * (Math.PI / 180.0);
            var num1 = point1.Longitude * (Math.PI / 180.0);
            var d2 = point2.Latitude * (Math.PI / 180.0);
            var num2 = point2.Longitude * (Math.PI / 180.0) - num1;
            var d3 = Math.Pow(Math.Sin((d2 - d1) / 2.0), 2.0) +
                     Math.Cos(d1) * Math.Cos(d2) * Math.Pow(Math.Sin(num2 / 2.0), 2.0);
            return 6376500.0 * (2.0 * Math.Atan2(Math.Sqrt(d3), Math.Sqrt(1.0 - d3)));
        }
    }

    public class Location
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }

    public class Notification
    {
        public string Device_ID { get; set; }
        public bool IsEnable { get; set; }
    }

    public class AppLanguage
    {
        public AppLanguageSelect Language { get; set; }
    }

    public enum AppLanguageSelect
    {
        [Description("Englist")]
        Englist = 1,
        [Description("Japanese")]
        Japanese = 2,
    }

    public enum AppFlag
    {
        [Description("CMS")]
        CMS = 1,
        [Description("SalesAdmin")]
        SalesAdmin = 2
    }

}
