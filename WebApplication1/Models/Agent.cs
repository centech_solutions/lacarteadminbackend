﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class Agent
    {
        public int Agent_ID { get; set; }
        public string ReferralID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ImageName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public int MaxDistance { get; set; }
        public string LastLocation { get; set; }
        public string LastUpdateLocation { get; set; }
        public int Status { get; set; }
    }

    public class AgentInfo
    {
        public int Agent_ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ImageName { get; set; }
        public string ImagePath { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public int MaxDistance { get; set; }
        public string LastLocation { get; set; }
        public string LastUpdateLocation { get; set; }
        public AgentStatus Status { get; set; }
        public string StatusText
        {
            get
            {
                return Status.ToDescription();
            }
        }
        public bool RestaurantActive { get; set; }
    }

    public class LoginAgent
    {
        public string PhoneNumber { get; set; }
        public string Password { get; set; }
    }

    public class ResetPassword
    {
        public string PhoneNumber { get; set; }
        public string CurrentPassword { get; set; }
        public string Password { get; set; }
    }

    public class UserAgent
    {
        public int id { get; set; }
        public string username { get; set; }
        public string PhoneNumber { get; set; }
        public string userToken { get; set; }
    }

    public class SignUpAgent
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public string ReferralID { get; set; }
        public string ImageName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public int MaxDistance { get; set; }
        public string LastLocation { get; set; }
        public string LastUpdateLocation { get; set; }
        public int Status { get; set; }
    }

    public class OrderAgent
    {
        public int Id { get; set; }
        public string TableNumber { get; set; }
        public string Customer { get; set; }
        public string Item { get; set; }
    }

    public class ListOrderByAgent
    {
        public List<OrderForAgent> ListCurrentOrder { get; set; }
        public List<OrderForAgent> ListPastOrder { get; set; }
    }

    public class OrderForAgent
    {
        public int ID { get; set; }
        public string Order_No { get; set; }
        public int RES_ID { get; set; }
        public string Firstname { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string LocationRestaurant { get; set; }
        public string LocationCustomer { get; set; }
        public string LocationAgent { get; set; }
        public string LocationOrder { get; set; }
        public string AddressOrder { get; set; }
        public string AddressRestaurant { get; set; }
        public string Image { get; set; }
        public string RestaurantLocation { get; set; }
        public int CUS_ID { get; set; }
        public string CustomerLocation { get; set; }
        public decimal Distance { get; set; }
        public double DistanceKilometers { get { return (double)Distance / 1000; } }
        public int? Agent_ID { get; set; }
        public OrderType Order_Type { get; set; }
        public string Order_TypeText
        {
            get
            {
                return Order_Type.ToDescription();
            }
        }
        public int? DineIn_Number { get; set; }
        public int CookingTime { get; set; }
        public decimal SubTotal { get; set; }
        public decimal Tax { get; set; }
        public decimal TransactionFee { get; set; }
        public decimal Total { get; set; }
        public DateTime? Accept_Time { get; set; }
        public DateTime? Cooked_Time { get; set; }
        public DateTime? Delivery_Start { get; set; }
        public DateTime? Delivery_End { get; set; }
        public OrderStatus Status { get; set; }
        public string StatusText
        {
            get
            {
                return Status.ToDescription();
            }
        }
        public AgentOrderStatus AgentStatus { get; set; }
        public string AgentStatusText
        {
            get
            {
                return AgentStatus.ToDescription();
            }
        }
        public OrderPayment PaymentMethod { get; set; }
        public string PaymentMethodText
        {
            get
            {
                return PaymentMethod.ToDescription();
            }
        }
        public string Comment { get; set; }
        public DateTime CreatedDate { get; set; }
        public List<OrderItem> ListOrderItems { get; set; }
    }

    public class OrderItem
    {
        public int Order_ID { get; set; }
        public int OrderItem_ID { get; set; }
        public int MenuItem_ID { get; set; }
        public Decimal PriceOrder { get; set; }
        public int AmountOrder { get; set; }
        public string Comment { get; set; }
        public string CreateDateOrder { get; set; }
        public string Name_EN { get; set; }
        public string Name_JP { get; set; }
        public string Desc_EN { get; set; }
        public string Desc_JP { get; set; }
        public int PercentTax { get; set; }
        public Decimal PriceItem { get; set; }
        public int CookingTime { get; set; }
    }

    public class MasterIssue
    {
        public int ISSUE_ID { get; set; }
        public string ISSUE_NAME { get; set; }
        public string ISSUE_NAME_JP { get; set; }
        public int Status { get; set; }
    }

    public class AgentIssue
    {
        public int Agent_ID { get; set; }
        public int ISSUE_ID { get; set; }
        public string Message { get; set; }
    }

    public class AgentDevice
    {
        public int Agent_ID { get; set; }
        public string Device_ID { get; set; }
    }

    public class AgentImage
    {
        public int Agent_ID { get; set; }
        public IFormFile Image { get; set; }
    }

    public class RestaurantAgent
    {
        public int Agent_ID { get; set; }
        public string PhoneNumber { get; set; }
        public int Status { get; set; }
        public string Referral_ID { get; set; }
        public int RES_ID { get; set; }
        public DateTime ExpireDate { get; set; }
        public int IsEnable { get; set; }
    }

    public enum AgentStatus
    {
        [Description("InActive")]
        InActive = 0,
        [Description("Active")]
        Active = 1,
        [Description("Delivery")]
        Delivery = 2
    }
}
