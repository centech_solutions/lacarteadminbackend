﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class MenuHour
    {
        public int ID { get; set; }
        public int RES_ID { get; set; }
        public int Sequence { get; set; }
        public string Name_EN { get; set; }
        public string Name_JP { get; set; }
        public string Desc_EN { get; set; }
        public string Desc_JP { get; set; }
        public bool IsSun { get; set; }
        public bool IsMon { get; set; }
        public bool IsTue { get; set; }
        public bool IsWed { get; set; }
        public bool IsThu { get; set; }
        public bool IsFri { get; set; }
        public bool IsSat { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string StartTime2 { get; set; }
        public string EndTime2 { get; set; }
        public string StartTime3 { get; set; }
        public string EndTime3 { get; set; }
        public string StartTime4 { get; set; }
        public string EndTime4 { get; set; }
        public string StartTime5 { get; set; }
        public string EndTime5 { get; set; }
        public string StartTime6 { get; set; }
        public string EndTime6 { get; set; }
        public string StartTime7 { get; set; }
        public string EndTime7 { get; set; }
        public TaxType TaxType { get; set; }
    }


    public class ViewMenuHour2
    {
        public int ID { get; set; }
        public int Sequence { get; set; }
        public string Name_EN { get; set; }
        public string Name_JP { get; set; }
        public string Desc_EN { get; set; }
        public string Desc_JP { get; set; }
        public bool IsSun { get; set; }
        public bool IsMon { get; set; }
        public bool IsTue { get; set; }
        public bool IsWed { get; set; }
        public bool IsThu { get; set; }
        public bool IsFri { get; set; }
        public bool IsSat { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string StartTime2 { get; set; }
        public string EndTime2 { get; set; }
        public string StartTime3 { get; set; }
        public string EndTime3 { get; set; }
        public string StartTime4 { get; set; }
        public string EndTime4 { get; set; }
        public string StartTime5 { get; set; }
        public string EndTime5 { get; set; }
        public string StartTime6 { get; set; }
        public string EndTime6 { get; set; }
        public string StartTime7 { get; set; }
        public string EndTime7 { get; set; }
        public DateTime CreatedDate { get; set; }
        public List<MenuGroup> MenuGroups { get; set; }
    }

    public class MenuGroup
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int MenuHour_ID { get; set; }
        public int Sequence { get; set; }
    }

    public class PutMenuItem
    {
        public int RES_ID { get;  set; }
        public int ID { get; set; }
        public string Name_EN { get; set; }
        public string Desc_EN { get; set; }
        public string Name_JP { get; set; }
        public string Desc_JP { get; set; }
        public int? PercentTax { get; set; }
        public int? Price { get; set; }
        public bool? Pickup { get; set; }
        public bool? Delivery { get; set; }
        public bool? DineIn { get; set; }
        public string ImageM { get; set; }
        public string ImageL { get; set; }
        public int? CookingTime { get; set; }
        public int MenuGroup_ID { get; internal set; }
        public int? Sequence { get; set; }
        public List<PutAddOn> Addons { get; set; }

    }
    
    public class PutAddOn
    {
        public int ID { get; set; }
        public int RES_ID { get; set; }
        public string Name_EN { get; set; }
        public string Name_JP { get; set; }
        public bool? Require { get; set; }
        public int MenuItem_ID { get; set; }
        public int? OptionCount { get; set; }
        public bool? IsEnable { get; set; }
        public List<PutAddOnOption> Options { get; set; }
    }

    public class PutAddOnOption
    {
        public int ID { get; set; }
        public int AddOn_ID { get; set; }
        public string Name_EN { get; set; }
        public string Name_JP { get; set; }
        public int? Price { get; set; }
        public bool? IsEnable { get; set; }
    }

    public enum TaxType
    {
        [Description("Exclusive")]
        Exclusive = 1,
        [Description("Inclusive")]
        Inclusive = 2,
    }
    public class MenuExcel
    {
        public int ID { get; set; }
        public int RES_ID { get; set; }
        public string NameMenuEN { get; set; }
        public string NameMenuJP { get; set; }
        public string Deteil_MenuEN { get; set; }
        public string Deteil_MenuJP { get; set; }
        public string Name_Category { get; set; }
        public string Name_ItemEN { get; set; }
        public string Name_ItemJP { get; set; }
        public bool IsSun { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public bool IsMon { get; set; }
        public string StartTime2 { get; set; }
        public string EndTime2 { get; set; }
        public bool IsTue { get; set; }
        public string StartTime3 { get; set; }
        public string EndTime3 { get; set; }
        public bool IsWed { get; set; }
        public string StartTime4 { get; set; }
        public string EndTime4 { get; set; }
        public bool IsThu { get; set; }
        public string StartTime5 { get; set; }
        public string EndTime5 { get; set; }
        public bool IsFri { get; set; }
        public string StartTime6 { get; set; }
        public string EndTime6 { get; set; }
        public bool IsSat { get; set; }
        public string StartTime7 { get; set; }
        public string EndTime7 { get; set; }
        public bool Delivery { get; set; }
        public bool Pickup { get; set; }
        public bool DineIn { get; set; }
        public DateTime CreatedDate { get; set; }
        
    }
}
