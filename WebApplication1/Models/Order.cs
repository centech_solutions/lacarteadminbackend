using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{

    public class Order
    {
        public int ID { get; set; }
        public string Order_No { get; set; }
        public int RES_ID { get; set; }
        public RestaurantInfo Restaurant { get; set; }
        public string RestaurantLocation { get; set; }
        public int CUS_ID { get; set; }
        public string CustomerLocation { get; set; }
        public decimal Distance { get; set; }
        public double DistanceKilometers { get { return (double)Distance / 1000; } }
        public int SubTotal { get; set; }
        public int Tax { get; set; }
        public int DeliveryFee { get; set; }
        public int TransactionFee { get; set; }
        public int TransactionFee_Percent { get; set; }
        public int Total { get; set; }
        public int? Agent_ID { get; set; }
        public AgentOrderStatus AgentStatus { get; set; }
        public string AgentStatusText
        {
            get
            {
                return AgentStatus.ToDescription();
            }
        }
        public OrderType Order_Type { get; set; }
        public string Order_TypeText
        {
            get
            {
                return Order_Type.ToDescription();
            }
        }
        public int? DineIn_Number { get; set; }
        public DineInServices DineIn_Service { get; set; }
        public OrderStatus Status { get; set; }
        public string StatusText
        {
            get
            {
                return Status.ToDescription();
            }
        }
        public OrderPayment PaymentMethod { get; set; }
        public string PaymentMethodText
        {
            get
            {
                return PaymentMethod.ToDescription();
            }
        }
        public bool PaymentStatus { get; set; }
        public string Payment_ID { get; set; }
        public DateTime CreatedDate { get; set; }
        public List<ViewOrderItem> Items { get; set; }

    }

    public class OrderDetail
    {
        public int ID { get; set; }
        public string Order_No { get; set; }
        public int RES_ID { get; set; }
        public string RestaurantLocation { get; set; }
        public int CUS_ID { get; set; }
        public string CustomerLocation { get; set; }
        public decimal Distance { get; set; }
        public double DistanceKilometers { get { return (double)Distance / 1000; } }
        public int? Agent_ID { get; set; }
        public AgentOrderStatus AgentStatus { get; set; }
        public string AgentStatusText
        {
            get
            {
                return AgentStatus.ToDescription();
            }
        }
        public AgentInfo Agent { get; set; }
        public OrderType Order_Type { get; set; }
        public string Order_TypeText
        {
            get
            {
                return Order_Type.ToDescription();
            }
        }
        public int? DineIn_Number { get; set; }
        public DineInServices DineIn_Service { get; set; }
        public int CookingTime { get; set; }
        public int SubTotal { get; set; }
        public int Tax { get; set; }
        public int DeliveryFee { get; set; }
        public int TransactionFee { get; set; }
        public int TransactionFee_Percent { get; set; }
        public int Total { get; set; }
        public DateTime? Accept_Time { get; set; }
        public DateTime? Cooked_Time { get; set; }
        public DateTime? Delivery_Start { get; set; }
        public DateTime? Delivery_End { get; set; }
        public DateTime? Complete_Time { get; set; }
        public OrderStatus Status { get; set; }
        public string StatusText
        {
            get
            {
                return Status.ToDescription();
            }
        }
        public OrderPayment PaymentMethod { get; set; }
        public string PaymentMethodText
        {
            get
            {
                return PaymentMethod.ToDescription();
            }
        }
        public bool PaymentStatus { get; set; }
        public string Payment_ID { get; set; }
        public string Comment { get; set; }
        public DateTime CreatedDate { get; set; }
        public OrderAddress OrderAddress { get; set; }
        public OrderRate OrderRate { get; set; }
        public List<ViewOrderItem> Items { get; set; }

    }

    public class ViewCustomerOrderDetail
    {
        public int ID { get; set; }
        public string Order_No { get; set; }
        public int RES_ID { get; set; }
        public RestaurantInfo Restaurant { get; set; }
        public string RestaurantLocation { get; set; }
        public int CUS_ID { get; set; }
        public string CustomerLocation { get; set; }
        public decimal Distance { get; set; }
        public double DistanceKilometers { get { return (double)Distance / 1000; } }
        public int? Agent_ID { get; set; }
        public AgentInfo Agent { get; set; }
        public AgentOrderStatus AgentStatus { get; set; }
        public string AgentStatusText
        {
            get
            {
                return AgentStatus.ToDescription();
            }
        }
        public OrderType Order_Type { get; set; }
        public string Order_TypeText
        {
            get
            {
                return Order_Type.ToDescription();
            }
        }
        public int? DineIn_Number { get; set; }
        public DineInServices DineIn_Service { get; set; }
        public string DineIn_ServiceText
        {
            get
            {
                return DineIn_Service.ToDescription();
            }
        }
        public int CookingTime { get; set; }
        public int SubTotal { get; set; }
        public int Tax { get; set; }
        public int DeliveryFee { get; set; }
        public int TransactionFee_Percent { get; set; }
        public int TransactionFee { get; set; }
        public int Total { get; set; }
        public DateTime? Accept_Time { get; set; }
        public DateTime? Cooked_Time { get; set; }
        public DateTime? Delivery_Start { get; set; }
        public DateTime? Delivery_End { get; set; }
        public DateTime? Complete_Time { get; set; }
        public OrderStatus Status { get; set; }
        public string StatusText
        {
            get
            {
                return Status.ToDescription();
            }
        }
        public OrderPayment PaymentMethod { get; set; }
        public string PaymentMethodText
        {
            get
            {
                return PaymentMethod.ToDescription();
            }
        }
        public bool PaymentStatus { get; set; }
        public string Payment_ID { get; set; }
        public string Comment { get; set; }
        public DateTime CreatedDate { get; set; }
        public OrderAddress OrderAddress { get; set; }
        public List<ViewOrderItem> Items { get; set; }

    }

    public class ViewOrderItem
    {
        public int ID { get; set; }
        public string Name_EN { get; set; }
        public string Name_JP { get; set; }
        public string Desc_EN { get; set; }
        public string Desc_JP { get; set; }
        public int Price { get; set; }
        public int Amount { get; set; }
        public string Comment { get; set; }
        public int MenuGroup_ID { get; set; }
        public List<ViewAddOnOrderItem> AddOns { get; set; }
    }

    public class ViewAddOnOrderItem
    {
        public int ID { get; set; }
        public int OrderItem_ID { get; set; }
        public int MenuItem_ID { get; set; }
        public string Name_EN { get; set; }
        public string Name_JP { get; set; }
        public List<ViewAddOnOrderOption> Options { get; set; }
    }

    public class ViewAddOnOrderOption
    {
        public int ID { get; set; }
        public int OrderItem_ID { get; set; }
        public int AddOn_ID { get; set; }
        public string Name_EN { get; set; }
        public string Name_JP { get; set; }
        public int Price { get; set; }
    }

    public class OrderAddress
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string PostCode { get; set; }
        public string Comment { get; set; }
        public string PhoneNumber { get; set; }
    }

    public class CreateOrder
    {
        public int RES_ID { get; set; }
        public OrderType Order_Type { get; set; }
        public OrderPayment PaymentMethod { get; set; }
        public int DineIn_Number { get; set; }
        public DineInServices DineIn_Service { get; set; }
        public int AddressID { get; set; }
        public decimal DeliveryFee { get; set; }
        public List<CreateOrderItem> Items { get; set; }
    }

    public class CreateOrderItem
    {
        public int MenuItem_ID { get; set; }
        public int Amount { get; set; }
        public string Comment { get; set; }
        public List<CreateOrderItemAddOn> AddOns { get; set; }

    }

    public class CreateOrderItemAddOn
    {
        public int AddOnOption_ID { get; set; }
    }

    public class AcceptOrder
    {
        public int Order_ID { get; set; }
        public int CookingTime { get; set; }
    }

    public class AssignAgent
    {
        public int Order_ID { get; set; }
        public int? Agent_ID { get; set; }
        public bool ManualDelivery { get; set; }
    }

    public class ChangeStatusOrder
    {
        public int Order_ID { get; set; }
        public int RES_ID { get; set; }
    }

    public class OrderRate
    {
        public int Order_ID { get; set; }
        public int FoodRate { get; set; }
        public string FoodComment { get; set; }
        public int AgentRate { get; set; }
        public string AgentComment { get; set; }
    }

    public enum OrderStatus
    {
        [Description("NEW")]
        New = 1,
        [Description("ACCEPTED")]
        Accept = 2,
        [Description("COOKING")]
        Cooking = 3,
        [Description("COOKED")]
        Cooked = 4,
        [Description("READY")]
        Ready = 5,
        [Description("WAITING")]
        Waiting = 6,
        [Description("ENROUTE")]
        Enroute = 7,
        [Description("UNPAID")]
        Unpaid = 8,
        [Description("PAID")]
        Paid = 9,
        [Description("COMPLETED")]
        Complete = 20,
        [Description("CANCELLED")]
        CustomerCancel = 98,
        [Description("CANCELLED")]
        RestaurantCancel = 99,
    }

    public enum OrderType
    {
        [Description("Delivery")]
        Delivery = 1,
        [Description("Take-out")]
        Pickup = 2,
        [Description("Dine-in")]
        Dinein = 3,
    }

    public enum OrderHistoryType
    {
        [Description("Daily")]
        Daily = 1,
        [Description("Weekly")]
        Weekly = 2,
        [Description("Monthly")]
        Monthly = 3,
    }


    public enum AgentOrderStatus
    {
        [Description("UnAssign")]
        UnAssign = 0,
        [Description("Assigned")]
        Assigned = 1,
        [Description("Accepted")]
        Accepted = 2,
        [Description("Received")]
        Received = 3,
    }

    public enum OrderPayment
    {
        [Description("Cash")]
        Cash = 1,
        [Description("CreditCard")]
        Credit = 2,
        [Description("Other")]
        Other = 3,
    }

    public enum DineInServices
    {
        [Description("")]
        Null = 0,
        [Description("PayNow")]
        Paynow = 1,
        [Description("PayLater")]
        Paylater = 2,
    }
    public class OrderHistory
    {
        public List<OrderCurrent> OrderCurrents { get; set; }
        public int OrderCurrentSum { get; set; }
        public List<OrderCancel> OrderCancels { get; set; }
        public int OrderCancelSum { get; set; }
        public List<OrderCompleted> OrderCompletes { get; set; }
        public int OrderCompleteSum { get; set; }
        public List<OrderPending> OrderPendings { get; set; }
        public int OrderPendingSum { get; set; }
    }
    public class OrderCurrent
    {
        public string Date { get; set; }
        public int RES_ID { get; set; }
        public string RestaurantNameEN { get; set; }
        public string RestaurantNameJP { get; set; }
        public int OrderCurrentCount { get; set; }
    }
    public class OrderCancel
    {
        public string Date { get; set; }
        public int RES_ID { get; set; }
        public string RestaurantNameEN { get; set; }
        public string RestaurantNameJP { get; set; }
        public int OrderCancelledCount { get; set; }
    }
    public class OrderCompleted
    {
        public string Date { get; set; }
        public int RES_ID { get; set; }
        public string RestaurantNameEN { get; set; }
        public string RestaurantNameJP { get; set; }
        public int OrderCompletedCount { get; set; }
    }
    public class OrderPending
    {
        public string Date { get; set; }
        public int RES_ID { get; set; }
        public string RestaurantNameEN { get; set; }
        public string RestaurantNameJP { get; set; }
        public int OrderPendingCount { get; set; }
    }

}
