﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class DumpMenuItem
    {
        public int RES_ID { get; set; }
        public int Category_ID { get; set; }
        public List<ExcelMenuItem> Items { get; set; }
    }

    public class ExcelMenuItem
    {
        public int No { get; set; }
        public string Name_EN { get; set; }
        public string Description_EN { get; set; }
        public string Name_JP { get; set; }
        public string Description_JP { get; set; }
        public int Price { get; set; }
        public bool Delivery { get; set; }
        public bool Pickup { get; set; }
        public bool DineIn { get; set; }
        public int CookingTime { get; set; }
    }

    public class DumpAddon
    {
        public int RES_ID { get; set; }
        public List<ExcelAddon> Addons { get; set; }
    }

    public class ExcelAddon
    {
        public int No { get; set; }
        public int MenuItemNo { get; set; }
        public string AddOnName_JP { get; set; }
        public string AddOnName_EN { get; set; }
        public int OptionsChoose { get; set; }
        public bool Require { get; set; }
    }

    public class ViewExcelAddon
    {
        public int No { get; set; }
        public int MenuItemNo { get; set; }
        public string MenuItemName_JP { get; set; }
        public string AddOnName_JP { get; set; }
        public string AddOnName_EN { get; set; }
        public int OptionsChoose { get; set; }
        public bool Require { get; set; }
        public bool Status { get { return MenuItemName_JP != null; } }
    }

    public class DumpAddonOption
    {
        public int RES_ID { get; set; }
        public List<ExcelAddonOption> AddonOptions { get; set; }
    }

    public class ExcelAddonOption
    {
        public int AddonNo { get; set; }
        public string Name_JP { get; set; }
        public string Name_EN { get; set; }
        public int Price { get; set; }
    }

    public class ViewExcelAddonOption
    {
        public int AddonNo { get; set; }
        public string AddonName_JP { get; set; }
        public string Name_JP { get; set; }
        public string Name_EN { get; set; }
        public int Price { get; set; }
        public bool Status { get { return AddonName_JP != null; } }
    }

    public class ConfirmDumpExcel
    {
        public int RES_ID { get; set; }
        public bool Status { get; set; }
    }
}
